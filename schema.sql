--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sign_group_memberships; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_group_memberships (
    sign_desig text NOT NULL,
    sign_group_id integer NOT NULL,
    sign_asset integer
);


ALTER TABLE public.sign_group_memberships OWNER TO cecsinet_dev;

--
-- Name: COLUMN sign_group_memberships.sign_asset; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_group_memberships.sign_asset IS 'Current asset holding this designator';


--
-- Name: sign_cfg; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_cfg (
    cfg_desig text NOT NULL,
    cfg_dataserver integer DEFAULT 1 NOT NULL,
    cfg_display_name text,
    cfg_reload boolean DEFAULT false NOT NULL,
    cfg_content_schedule integer,
    cfg_report_data boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sign_cfg OWNER TO cecsinet_dev;

--
-- Name: sign_groups; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_groups (
    group_id integer NOT NULL,
    group_name text NOT NULL,
    group_layout integer,
    group_priv integer,
    group_width integer NOT NULL,
    group_height integer NOT NULL,
    group_layout_alternate integer,
    group_layout_fullscreen integer,
    CONSTRAINT sign_groups_chk CHECK (((group_width > 0) AND (group_height > 0)))
);


ALTER TABLE public.sign_groups OWNER TO cecsinet_dev;

--
-- Name: sign_group_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_group_view AS
         SELECT sg.group_id,
            sg.group_name,
            sg.group_layout,
            dna.alias_name AS group_sign,
            sg.group_layout_alternate,
            sg.group_layout_fullscreen,
            sc.cfg_display_name AS sign_display
           FROM ((sign_groups sg
      LEFT JOIN ip_dns_alias dna ON ((sign_group_id(dna.alias_name) = sg.group_id)))
   LEFT JOIN sign_cfg sc ON ((sc.cfg_desig = dna.alias_name)))
UNION
         SELECT sg.group_id,
            sg.group_name,
            sg.group_layout,
            sc.cfg_desig AS group_sign,
            sg.group_layout_alternate,
            sg.group_layout_fullscreen,
            sc.cfg_display_name AS sign_display
           FROM (sign_groups sg
      LEFT JOIN sign_cfg sc ON ((sign_group_id(sc.cfg_desig) = sg.group_id)))
     WHERE (NOT (EXISTS ( SELECT dna.alias_id
              FROM ip_dns_alias dna
             WHERE ((sign_group_id(dna.alias_name) = sg.group_id) AND (dna.alias_name = sc.cfg_desig)))));


ALTER TABLE public.sign_group_view OWNER TO cecsinet_dev;

--
-- Name: sign_all_layouts_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_all_layouts_view AS
        (         SELECT sgv.group_sign AS sign,
                    sgv.group_layout AS layout,
                    true AS is_primary,
                    false AS is_fullscreen
                   FROM sign_group_view sgv
        UNION
                 SELECT sgv.group_sign AS sign,
                    sgv.group_layout_alternate AS layout,
                    false AS is_primary,
                    false AS is_fullscreen
                   FROM sign_group_view sgv
                  WHERE (sgv.group_layout_alternate IS NOT NULL))
UNION
         SELECT sgv.group_sign AS sign,
            sgv.group_layout_fullscreen AS layout,
            false AS is_primary,
            true AS is_fullscreen
           FROM sign_group_view sgv
          WHERE (sgv.group_layout_fullscreen IS NOT NULL);


ALTER TABLE public.sign_all_layouts_view OWNER TO cecsinet_dev;

--
-- Name: sign_layouts; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_layouts (
    layout_id integer NOT NULL,
    layout_title text NOT NULL,
    layout_backgrounds text[] NOT NULL,
    layout_rotate_bg boolean DEFAULT true NOT NULL,
    layout_width integer NOT NULL,
    layout_height integer NOT NULL,
    CONSTRAINT sign_layouts_chk CHECK (((layout_width > 0) AND (layout_height > 0)))
);


ALTER TABLE public.sign_layouts OWNER TO cecsinet_dev;

--
-- Name: COLUMN sign_layouts.layout_rotate_bg; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_layouts.layout_rotate_bg IS 'If do_rotate, then sign rotates every refresh-- otherwise, uses the same layout until it dies';


--
-- Name: sign_region2layout; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_region2layout (
    inst_layout_id integer NOT NULL,
    inst_region_id bigint NOT NULL,
    inst_bgcolor text,
    inst_top integer,
    inst_right integer,
    inst_bottom integer,
    inst_left integer,
    CONSTRAINT sign_region2layout_rect_chk CHECK ((((((xor((inst_top IS NOT NULL), (inst_bottom IS NOT NULL)) AND xor((inst_right IS NOT NULL), (inst_left IS NOT NULL))) AND (COALESCE(inst_top, 0) >= 0)) AND (COALESCE(inst_right, 0) >= 0)) AND (COALESCE(inst_bottom, 0) >= 0)) AND (COALESCE(inst_left, 0) >= 0)))
);


ALTER TABLE public.sign_region2layout OWNER TO cecsinet_dev;

--
-- Name: sign_regions; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_regions (
    region_id bigint NOT NULL,
    region_name text NOT NULL,
    region_width integer,
    region_height integer,
    region_default_bgcolor text,
    region_type integer DEFAULT 1 NOT NULL,
    region_default_fgcolor text,
    region_default_fsize double precision,
    region_default_lheight double precision,
    region_privileged boolean DEFAULT false NOT NULL,
    region_priv integer,
    region_backup bigint,
    region_css text,
    region_overlays_backup boolean DEFAULT false NOT NULL,
    region_children_css text,
    CONSTRAINT sign_regions_dim_chk CHECK ((((region_width IS NULL) OR (region_width > 0)) AND ((region_height IS NULL) OR (region_height > 0)))),
    CONSTRAINT sign_regions_fsize_chk CHECK ((region_default_fsize > (0)::double precision))
);


ALTER TABLE public.sign_regions OWNER TO cecsinet_dev;

--
-- Name: COLUMN sign_regions.region_default_fsize; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_regions.region_default_fsize IS 'Font size in pixels';


--
-- Name: COLUMN sign_regions.region_default_lheight; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_regions.region_default_lheight IS 'Line height in pixels';


--
-- Name: COLUMN sign_regions.region_backup; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_regions.region_backup IS 'Two levels of backup regions will *not* work and is *not* supported';


--
-- Name: COLUMN sign_regions.region_overlays_backup; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_regions.region_overlays_backup IS 'Specifies whether a region should overlay the backup region or override the backup region - the latter is the default behavior.';


--
-- Name: sign_layout_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_layout_view AS
 SELECT sg.group_sign AS designator,
    sg.group_id,
    sl.layout_id,
    sl.layout_title,
    sl.layout_backgrounds,
    sl.layout_rotate_bg,
    sl.layout_width,
    sl.layout_height,
    ( SELECT bool_or(((sr.region_priv IS NULL) OR cur_user_has_view_priv(sr.region_priv))) AS bool_or
           FROM (sign_region2layout r2l
      LEFT JOIN sign_regions sr ON ((sr.region_id = r2l.inst_region_id)))
     WHERE (r2l.inst_layout_id = sl.layout_id)) AS layout_usable,
    slv.is_primary AS layout_primary,
    slv.is_fullscreen AS layout_fullscreen
   FROM ((sign_group_view sg
   LEFT JOIN sign_all_layouts_view slv ON ((slv.sign = sg.group_sign)))
   LEFT JOIN sign_layouts sl ON ((sl.layout_id = slv.layout)))
  WHERE (sg.group_sign IS NOT NULL);


ALTER TABLE public.sign_layout_view OWNER TO cecsinet_dev;

--
-- Name: sign_region_types; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_region_types (
    type_id integer NOT NULL,
    type_desc text NOT NULL,
    type_class text
);


ALTER TABLE public.sign_region_types OWNER TO cecsinet_dev;

--
-- Name: sign_region_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_region_view AS
 SELECT slv.designator,
    slv.group_id,
    slv.layout_id,
    sr.region_id,
    (((sr.region_priv IS NOT NULL) AND (NOT cur_user_has_priv(sr.region_priv))) AND (NOT cur_user_has_priv('SIGN_ADMIN'::text))) AS privileged,
    r2l.inst_top AS top,
    r2l.inst_right AS "right",
    r2l.inst_bottom AS bottom,
    r2l.inst_left AS "left",
    sr.region_name AS name,
    srt.type_desc AS "desc",
    srt.type_class AS class,
    sr.region_width AS width,
    sr.region_height AS height,
    COALESCE(r2l.inst_bgcolor, sr.region_default_bgcolor) AS bgcolor,
    sr.region_default_fgcolor AS fgcolor,
    sr.region_default_fsize AS fontsize,
    sr.region_default_lheight AS lineheight,
    sr.region_backup AS backup,
    sr.region_css AS css,
    sr.region_overlays_backup AS overlays_backup,
    sr.region_children_css AS children_css
   FROM (((sign_layout_view slv
   LEFT JOIN sign_region2layout r2l ON ((r2l.inst_layout_id = slv.layout_id)))
   LEFT JOIN sign_regions sr ON ((sr.region_id = r2l.inst_region_id)))
   LEFT JOIN sign_region_types srt ON ((srt.type_id = sr.region_type)));


ALTER TABLE public.sign_region_view OWNER TO cecsinet_dev;

--
-- Name: sign_backup_region_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_backup_region_view AS
 SELECT sign_region_view.designator,
    sign_region_view.group_id,
    sign_region_view.layout_id,
    sign_region_view.region_id,
    sign_region_view.privileged,
    sign_region_view.top,
    sign_region_view."right",
    sign_region_view.bottom,
    sign_region_view."left",
    sign_region_view.name,
    sign_region_view."desc",
    sign_region_view.class,
    sign_region_view.width,
    sign_region_view.height,
    sign_region_view.bgcolor,
    sign_region_view.fgcolor,
    sign_region_view.fontsize,
    sign_region_view.lineheight,
    sign_region_view.backup
   FROM sign_region_view
  WHERE (sign_region_view.backup IS NOT NULL);


ALTER TABLE public.sign_backup_region_view OWNER TO cecsinet_dev;

--
-- Name: sign_data_types; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_data_types (
    type_id integer NOT NULL,
    type_name text NOT NULL,
    type_default_timeout integer DEFAULT 15 NOT NULL,
    type_display_name text,
    type_active boolean DEFAULT false NOT NULL,
    type_expects_file boolean DEFAULT false NOT NULL,
    type_expects_text boolean DEFAULT false NOT NULL,
    type_allow_timeout_change boolean DEFAULT false NOT NULL,
    CONSTRAINT sign_data_types_chk CHECK ((type_name ~ '^[a-z\-\_]+$'::text)),
    CONSTRAINT sign_data_types_timeout_chk CHECK ((type_default_timeout > 0))
);


ALTER TABLE public.sign_data_types OWNER TO cecsinet_dev;

--
-- Name: sign_published; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_published (
    published_id bigint NOT NULL,
    published_by integer DEFAULT get_cur_user() NOT NULL,
    published_at timestamp with time zone DEFAULT now() NOT NULL,
    published_title text NOT NULL,
    published_type integer NOT NULL,
    published_start timestamp with time zone NOT NULL,
    published_end timestamp with time zone,
    published_timeout integer,
    published_html text DEFAULT ''::text NOT NULL,
    published_html_revisions integer DEFAULT 0 NOT NULL,
    published_html_last_change timestamp(0) with time zone,
    published_last_refresh timestamp(0) with time zone,
    published_transition integer NOT NULL,
    published_tag text,
    CONSTRAINT sign_published_timeout_chk CHECK (((published_timeout IS NULL) OR (published_timeout > 0)))
);


ALTER TABLE public.sign_published OWNER TO cecsinet_dev;

--
-- Name: COLUMN sign_published.published_timeout; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_published.published_timeout IS 'Time to display in seconds';


--
-- Name: sign_published2group; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_published2group (
    group_id integer NOT NULL,
    published_id bigint NOT NULL,
    region_id bigint NOT NULL
);


ALTER TABLE public.sign_published2group OWNER TO cecsinet_dev;

--
-- Name: sign_published2sign_ex; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_published2sign_ex (
    sign_designator text NOT NULL,
    published_id bigint NOT NULL,
    region_id bigint NOT NULL
);


ALTER TABLE public.sign_published2sign_ex OWNER TO cecsinet_dev;

--
-- Name: sign_transitions; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_transitions (
    transition_id integer NOT NULL,
    transition_name text NOT NULL,
    transition_function text NOT NULL,
    transition_parameters text,
    transition_disabled boolean DEFAULT false NOT NULL,
    transition_default boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sign_transitions OWNER TO cecsinet_dev;

--
-- Name: COLUMN sign_transitions.transition_parameters; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON COLUMN sign_transitions.transition_parameters IS 'Expecting JSON';


--
-- Name: sign_all_content_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_all_content_view AS
         SELECT srv.designator,
            srv.layout_id,
            srv.group_id,
            srv.region_id,
            sp.published_id,
            sp.published_type,
            sdt.type_name AS published_type_name,
            srv.name AS region_name,
            sp.published_title AS title,
            sp.published_start AS start,
            sp.published_end AS "end",
            COALESCE(sp.published_timeout, sdt.type_default_timeout) AS timeout,
            sp.published_html AS html,
            sp.published_tag AS tag,
            sdt.type_display_name AS type_name,
            st.transition_function AS fx_func,
            st.transition_parameters AS fx_args,
            false AS is_backup
           FROM ((((sign_region_view srv
      LEFT JOIN sign_published2group p2g ON (((p2g.region_id = srv.region_id) AND (p2g.group_id = sign_group_id(srv.designator)))))
   LEFT JOIN sign_published sp ON ((sp.published_id = p2g.published_id)))
   LEFT JOIN sign_transitions st ON ((sp.published_transition = st.transition_id)))
   LEFT JOIN sign_data_types sdt ON ((sdt.type_id = sp.published_type)))
  WHERE (NOT (EXISTS ( SELECT true AS bool
   FROM sign_published2sign_ex p2s_e
  WHERE (((p2s_e.sign_designator = srv.designator) AND (p2s_e.published_id = p2g.published_id)) AND (p2s_e.region_id = p2g.region_id)))))
UNION
         SELECT DISTINCT srv.designator,
            srv.layout_id,
            srv.group_id,
            srv.region_id,
            sp.published_id,
            sp.published_type,
            sdt.type_name AS published_type_name,
            srv.name AS region_name,
            sp.published_title AS title,
            sp.published_start AS start,
            sp.published_end AS "end",
            COALESCE(sp.published_timeout, sdt.type_default_timeout) AS timeout,
            sp.published_html AS html,
            sp.published_tag AS tag,
            sdt.type_display_name AS type_name,
            st.transition_function AS fx_func,
            st.transition_parameters AS fx_args,
            true AS is_backup
           FROM ((((sign_backup_region_view srv
      LEFT JOIN sign_published2group p2g ON ((p2g.region_id = srv.backup)))
   LEFT JOIN sign_published sp ON ((sp.published_id = p2g.published_id)))
   LEFT JOIN sign_transitions st ON ((sp.published_transition = st.transition_id)))
   LEFT JOIN sign_data_types sdt ON ((sdt.type_id = sp.published_type)));


ALTER TABLE public.sign_all_content_view OWNER TO cecsinet_dev;

--
-- Name: sign_calendar; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_calendar (
    calendar_pkey bigint NOT NULL,
    calendar_published_id bigint NOT NULL,
    calendar_id bigint NOT NULL,
    calendar_by integer DEFAULT get_cur_user() NOT NULL,
    calendar_updated timestamp(0) with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.sign_calendar OWNER TO cecsinet_dev;

--
-- Name: sign_calendar_calendar_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_calendar_calendar_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_calendar_calendar_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_calendar_calendar_pkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_calendar_calendar_pkey_seq OWNED BY sign_calendar.calendar_pkey;


--
-- Name: sign_data_servers; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_data_servers (
    server_id integer NOT NULL,
    server_addr text NOT NULL,
    server_desc text NOT NULL,
    server_default boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sign_data_servers OWNER TO cecsinet_dev;

--
-- Name: sign_cfg_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_cfg_view AS
 SELECT sc.cfg_desig,
    ('https://'::text || sds.server_addr) AS dataserver,
    sgv.group_id,
    sc.cfg_display_name AS display,
    sc.cfg_reload AS reload,
    COALESCE(( SELECT true AS bool
           FROM ics_data icsd
          WHERE ((icsd.icsd_calendar = sc.cfg_content_schedule) AND ((date_trunc('minute'::text, icsd.icsd_start) = date_trunc('minute'::text, (now() + '00:01:00'::interval))) OR (date_trunc('minute'::text, icsd.icsd_end) = date_trunc('minute'::text, (now() + '00:01:00'::interval)))))), false) AS force_content,
    sc.cfg_report_data AS "reportData"
   FROM ((sign_cfg sc
   LEFT JOIN sign_data_servers sds ON ((sds.server_id = sc.cfg_dataserver)))
   LEFT JOIN sign_group_view sgv ON ((sgv.group_sign = sc.cfg_desig)));


ALTER TABLE public.sign_cfg_view OWNER TO cecsinet_dev;

--
-- Name: sign_commands; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_commands (
    cmd_id integer NOT NULL,
    cmd_manu integer NOT NULL,
    cmd_category integer NOT NULL,
    cmd_byte text NOT NULL
);


ALTER TABLE public.sign_commands OWNER TO cecsinet_dev;

--
-- Name: sign_commands_cmd_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_commands_cmd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_commands_cmd_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_commands_cmd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_commands_cmd_id_seq OWNED BY sign_commands.cmd_id;


--
-- Name: sign_content_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_content_view AS
 SELECT scv.designator,
    scv.layout_id,
    scv.group_id,
    scv.region_id,
    scv.published_id,
    scv.published_type,
    scv.published_type_name,
    scv.region_name,
    scv.title,
    scv.start,
    scv."end",
    scv.timeout,
    scv.html,
    scv.tag,
    scv.type_name,
    scv.fx_func,
    scv.fx_args,
    scv.is_backup
   FROM sign_all_content_view scv
  WHERE ((scv.start <= now()) AND ((scv."end" IS NULL) OR (scv."end" > now())));


ALTER TABLE public.sign_content_view OWNER TO cecsinet_dev;

--
-- Name: sign_data; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_data (
    data_desig text NOT NULL,
    data_reported timestamp with time zone DEFAULT now() NOT NULL,
    data_json text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.sign_data OWNER TO cecsinet_dev;

--
-- Name: sign_data_refresh_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_data_refresh_view AS
 SELECT sp.published_id,
    sdt.type_name AS published_type
   FROM (sign_published sp
   LEFT JOIN sign_data_types sdt ON ((sdt.type_id = sp.published_type)))
  WHERE ((sp.published_end IS NULL) OR ((sp.published_end)::date >= ('now'::text)::date));


ALTER TABLE public.sign_data_refresh_view OWNER TO cecsinet_dev;

--
-- Name: sign_data_servers_server_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_data_servers_server_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_data_servers_server_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_data_servers_server_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_data_servers_server_id_seq OWNED BY sign_data_servers.server_id;


--
-- Name: sign_data_types_type_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_data_types_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_data_types_type_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_data_types_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_data_types_type_id_seq OWNED BY sign_data_types.type_id;


--
-- Name: sign_def_cat; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_def_cat (
    cat_id integer NOT NULL,
    cat_name text NOT NULL,
    cat_strict boolean DEFAULT true NOT NULL,
    cat_label text,
    cat_check boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sign_def_cat OWNER TO cecsinet_dev;

--
-- Name: sign_def_cat_cat_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_def_cat_cat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_def_cat_cat_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_def_cat_cat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_def_cat_cat_id_seq OWNED BY sign_def_cat.cat_id;


--
-- Name: sign_def_op; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_def_op (
    op_id integer NOT NULL,
    op_category integer NOT NULL,
    op_category_index integer NOT NULL,
    op_attribute text NOT NULL,
    op_default boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sign_def_op OWNER TO cecsinet_dev;

--
-- Name: sign_def_op_op_category_index_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_def_op_op_category_index_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_def_op_op_category_index_seq OWNER TO cecsinet_dev;

--
-- Name: sign_def_op_op_category_index_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_def_op_op_category_index_seq OWNED BY sign_def_op.op_category_index;


--
-- Name: sign_def_op_op_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_def_op_op_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_def_op_op_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_def_op_op_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_def_op_op_id_seq OWNED BY sign_def_op.op_id;


--
-- Name: sign_defaults; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_defaults (
    sign_desig text NOT NULL,
    sign_option integer NOT NULL
);


ALTER TABLE public.sign_defaults OWNER TO cecsinet_dev;

--
-- Name: sign_group_defaults; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_group_defaults (
    sign_group_id integer NOT NULL,
    sign_option integer NOT NULL
);


ALTER TABLE public.sign_group_defaults OWNER TO cecsinet_dev;

--
-- Name: sign_group_defaults_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_group_defaults_view AS
 SELECT sign_groups.group_id,
    sign_groups.group_name,
    sign_def_cat.cat_id,
    sign_def_cat.cat_name,
    sign_def_cat.cat_strict,
    COALESCE(( SELECT sign_group_defaults.sign_option
           FROM sign_group_defaults
          WHERE ((sign_group_defaults.sign_group_id = sign_groups.group_id) AND (sign_group_defaults.sign_option IN ( SELECT sign_def_op.op_id
                   FROM sign_def_op
                  WHERE (sign_def_op.op_category = sign_def_cat.cat_id))))), ( SELECT sign_def_op.op_id
           FROM (sign_def_op
      LEFT JOIN sign_def_cat sign_def_cat_1 ON ((sign_def_cat_1.cat_id = sign_def_op.op_category)))
     WHERE (((sign_def_op.op_category = sign_def_cat_1.cat_id) AND (sign_def_cat_1.cat_strict IS TRUE)) AND (sign_def_cat.cat_id = sign_def_cat_1.cat_id))
     ORDER BY (sign_def_op.op_default IS TRUE) DESC, sign_def_op.op_category_index
    LIMIT 1)) AS op_id
   FROM (sign_groups
   LEFT JOIN sign_def_cat ON (true))
  WHERE true
  ORDER BY sign_groups.group_id, sign_def_cat.cat_id;


ALTER TABLE public.sign_group_defaults_view OWNER TO cecsinet_dev;

--
-- Name: sign_defaults_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_defaults_view AS
 SELECT sign_group_memberships.sign_group_id,
    sign_cfg_view.cfg_desig,
    sign_def_cat.cat_id,
    sign_def_cat.cat_name,
    sign_def_cat.cat_strict,
    COALESCE(( SELECT sign_defaults.sign_option
           FROM sign_defaults
          WHERE ((sign_defaults.sign_desig = sign_cfg_view.cfg_desig) AND (sign_defaults.sign_option IN ( SELECT sign_def_op.op_id
                   FROM sign_def_op
                  WHERE (sign_def_op.op_category = sign_def_cat.cat_id))))), ( SELECT sign_group_defaults_view.op_id
           FROM sign_group_defaults_view
          WHERE ((sign_group_defaults_view.cat_id = sign_def_cat.cat_id) AND (sign_group_defaults_view.group_id = sign_group_memberships.sign_group_id)))) AS op_id,
    (( SELECT sign_defaults.sign_option
           FROM sign_defaults
          WHERE ((sign_defaults.sign_desig = sign_cfg_view.cfg_desig) AND (sign_defaults.sign_option IN ( SELECT sign_def_op.op_id
                   FROM sign_def_op
                  WHERE (sign_def_op.op_category = sign_def_cat.cat_id))))) IS NULL) AS op_inherit
   FROM ((sign_cfg_view
   LEFT JOIN sign_def_cat ON (true))
   LEFT JOIN sign_group_memberships ON ((sign_cfg_view.cfg_desig = sign_group_memberships.sign_desig)))
  WHERE true
  ORDER BY sign_group_memberships.sign_group_id, sign_cfg_view.cfg_desig, sign_def_cat.cat_id;


ALTER TABLE public.sign_defaults_view OWNER TO cecsinet_dev;

--
-- Name: sign_event; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_event (
    event_pkey bigint NOT NULL,
    event_published_id integer NOT NULL,
    event_summary text NOT NULL,
    event_start timestamp with time zone NOT NULL,
    event_end timestamp with time zone NOT NULL
);


ALTER TABLE public.sign_event OWNER TO cecsinet_dev;

--
-- Name: sign_event_event_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_event_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_event_event_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_event_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_event_event_id_seq OWNED BY sign_event.event_pkey;


--
-- Name: sign_files; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_files (
    file_pkey bigint NOT NULL,
    file_published_id bigint NOT NULL,
    file_utime timestamp with time zone DEFAULT now() NOT NULL,
    file_by integer DEFAULT get_cur_user(),
    file_size bigint NOT NULL,
    file_name text NOT NULL,
    file_mime_type text NOT NULL,
    file_uploaded_name text NOT NULL,
    file_invalid boolean DEFAULT false NOT NULL,
    file_field text
);


ALTER TABLE public.sign_files OWNER TO cecsinet_dev;

--
-- Name: sign_files_file_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_files_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_files_file_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_files_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_files_file_id_seq OWNED BY sign_files.file_pkey;


--
-- Name: sign_files_file_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_files_file_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_files_file_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_future_content_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_future_content_view AS
 SELECT scv.designator,
    scv.layout_id,
    scv.group_id,
    scv.region_id,
    scv.published_id,
    scv.published_type,
    scv.published_type_name,
    scv.region_name,
    scv.title,
    scv.start,
    scv."end",
    scv.timeout,
    scv.html,
    scv.tag,
    scv.type_name,
    scv.fx_func,
    scv.fx_args
   FROM sign_all_content_view scv
  WHERE (scv.start > now());


ALTER TABLE public.sign_future_content_view OWNER TO cecsinet_dev;

--
-- Name: sign_groups_group_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_groups_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_groups_group_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_groups_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_groups_group_id_seq OWNED BY sign_groups.group_id;


--
-- Name: sign_lab_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_lab_view AS
 SELECT ('lab'::text || (ln.lab_group_name)::text) AS sign_lab_group,
    au.user_username AS sign_computersystem_name,
    comp.comp_asset_id AS sign_asset_id,
    COALESCE(dna.alias_name, comp_name_to_desig((au.user_username)::text)) AS sign_designator
   FROM (((((lab_name ln
   LEFT JOIN admin_group ag ON (((ag.group_name)::text ~~* ('lab'::text || (ln.lab_group_name)::text))))
   LEFT JOIN admin_user2group_effective effmem ON ((effmem.eff_group_id = ag.group_id)))
   LEFT JOIN admin_user au ON ((au.user_id = effmem.eff_user_id)))
   LEFT JOIN inv_computer comp ON (((lower(comp.comp_computersystem_name) || '$'::text) = lower((au.user_username)::text))))
   LEFT JOIN ip_dns_alias dna ON ((dna.alias_asset_id = comp.comp_asset_id)))
  WHERE (ln.lab_name = 'DigitalSigns'::text);


ALTER TABLE public.sign_lab_view OWNER TO cecsinet_dev;

--
-- Name: sign_layout; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_layout (
    layout_pkey integer NOT NULL,
    layout_published_id bigint NOT NULL,
    layout_target_layout integer NOT NULL,
    layout_target_sign text
);


ALTER TABLE public.sign_layout OWNER TO cecsinet_dev;

--
-- Name: sign_layout_img_cache; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_layout_img_cache (
    cache_id bigint NOT NULL,
    cache_creation timestamp with time zone DEFAULT now() NOT NULL,
    cache_user integer DEFAULT get_cur_user() NOT NULL,
    cache_layout_id integer NOT NULL,
    cache_mtime timestamp(0) with time zone NOT NULL,
    cache_width integer NOT NULL,
    cache_height integer NOT NULL,
    cache_data bytea NOT NULL
);


ALTER TABLE public.sign_layout_img_cache OWNER TO cecsinet_dev;

--
-- Name: sign_layout_img_cache_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_layout_img_cache_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_layout_img_cache_cache_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_layout_img_cache_cache_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_layout_img_cache_cache_id_seq OWNED BY sign_layout_img_cache.cache_id;


--
-- Name: sign_layout_layout_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_layout_layout_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_layout_layout_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_layout_layout_pkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_layout_layout_pkey_seq OWNED BY sign_layout.layout_pkey;


--
-- Name: sign_layouts_layout_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_layouts_layout_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_layouts_layout_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_layouts_layout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_layouts_layout_id_seq OWNED BY sign_layouts.layout_id;


--
-- Name: sign_officehours; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_officehours (
    officehours_pkey integer NOT NULL,
    officehours_published_id bigint NOT NULL,
    officehours_calendar_id bigint NOT NULL,
    officehours_by integer DEFAULT get_cur_user() NOT NULL,
    officehours_updated timestamp with time zone DEFAULT now() NOT NULL,
    officehours_days interval(0) DEFAULT '7 days'::interval NOT NULL
);


ALTER TABLE public.sign_officehours OWNER TO cecsinet_dev;

--
-- Name: sign_officehours_officehours_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_officehours_officehours_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_officehours_officehours_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_officehours_officehours_pkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_officehours_officehours_pkey_seq OWNED BY sign_officehours.officehours_pkey;


--
-- Name: sign_published_published_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_published_published_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_published_published_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_published_published_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_published_published_id_seq OWNED BY sign_published.published_id;


--
-- Name: sign_pull_layout_view; Type: VIEW; Schema: public; Owner: cecsinet_dev
--

CREATE VIEW sign_pull_layout_view AS
 SELECT sign_layout_view.designator,
    sign_layout_view.group_id,
    sign_layout_view.layout_id,
    sign_layout_view.layout_title,
    sign_layout_view.layout_backgrounds,
    sign_layout_view.layout_rotate_bg,
    sign_layout_view.layout_width,
    sign_layout_view.layout_height,
    sign_layout_view.layout_usable,
    sign_layout_view.layout_primary,
    sign_layout_view.layout_fullscreen,
    ( SELECT count(*) AS count
           FROM sign_content_view scv
          WHERE (((scv.designator = sign_layout_view.designator) AND (scv.layout_id = sign_layout_view.layout_id)) AND (NOT scv.is_backup))) AS layout_content_count
   FROM sign_layout_view;


ALTER TABLE public.sign_pull_layout_view OWNER TO cecsinet_dev;

--
-- Name: sign_region_types_type_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_region_types_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_region_types_type_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_region_types_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_region_types_type_id_seq OWNED BY sign_region_types.type_id;


--
-- Name: sign_regions_region_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_regions_region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_regions_region_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_regions_region_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_regions_region_id_seq OWNED BY sign_regions.region_id;


--
-- Name: sign_rss; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_rss (
    rss_pkey bigint NOT NULL,
    rss_published_id bigint NOT NULL,
    rss_last_pull text DEFAULT ''::text NOT NULL,
    rss_last_pull_time timestamp with time zone DEFAULT now() NOT NULL,
    rss_ttl integer,
    rss_title text,
    rss_description text,
    rss_link text
);


ALTER TABLE public.sign_rss OWNER TO cecsinet_dev;

--
-- Name: TABLE sign_rss; Type: COMMENT; Schema: public; Owner: cecsinet_dev
--

COMMENT ON TABLE sign_rss IS 'Contains channel information for this RSS feed';


--
-- Name: sign_rss_rss_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_rss_rss_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_rss_rss_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_rss_rss_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_rss_rss_id_seq OWNED BY sign_rss.rss_pkey;


--
-- Name: sign_rss_rss_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_rss_rss_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_rss_rss_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_text; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_text (
    text_pkey bigint NOT NULL,
    text_published_id bigint NOT NULL,
    text_by integer DEFAULT get_cur_user(),
    text_time timestamp(0) with time zone DEFAULT now() NOT NULL,
    text_value text NOT NULL,
    text_last_update timestamp(0) with time zone,
    text_field text,
    CONSTRAINT sign_text_val_chk CHECK ((length(btrim(text_value)) > 0))
);


ALTER TABLE public.sign_text OWNER TO cecsinet_dev;

--
-- Name: sign_text_text_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_text_text_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_text_text_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_text_text_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_text_text_id_seq OWNED BY sign_text.text_pkey;


--
-- Name: sign_transitions_transition_id_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_transitions_transition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_transitions_transition_id_seq OWNER TO cecsinet_dev;

--
-- Name: sign_transitions_transition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_transitions_transition_id_seq OWNED BY sign_transitions.transition_id;


--
-- Name: sign_twitter; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_twitter (
    twitter_pkey integer NOT NULL,
    twitter_published_id bigint NOT NULL,
    twitter_last_pull_time timestamp with time zone,
    twitter_lifespan interval DEFAULT '1 day'::interval NOT NULL,
    twitter_handle text NOT NULL,
    twitter_limit integer DEFAULT 10 NOT NULL
);


ALTER TABLE public.sign_twitter OWNER TO cecsinet_dev;

--
-- Name: sign_twitter_twitter_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_twitter_twitter_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_twitter_twitter_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_twitter_twitter_pkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_twitter_twitter_pkey_seq OWNED BY sign_twitter.twitter_pkey;


--
-- Name: sign_weather; Type: TABLE; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE TABLE sign_weather (
    weather_pkey integer NOT NULL,
    weather_published_id bigint,
    weather_days integer DEFAULT 3 NOT NULL
);


ALTER TABLE public.sign_weather OWNER TO cecsinet_dev;

--
-- Name: sign_weather_weather_pkey_seq; Type: SEQUENCE; Schema: public; Owner: cecsinet_dev
--

CREATE SEQUENCE sign_weather_weather_pkey_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sign_weather_weather_pkey_seq OWNER TO cecsinet_dev;

--
-- Name: sign_weather_weather_pkey_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cecsinet_dev
--

ALTER SEQUENCE sign_weather_weather_pkey_seq OWNED BY sign_weather.weather_pkey;


--
-- Name: calendar_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_calendar ALTER COLUMN calendar_pkey SET DEFAULT nextval('sign_calendar_calendar_pkey_seq'::regclass);


--
-- Name: cmd_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_commands ALTER COLUMN cmd_id SET DEFAULT nextval('sign_commands_cmd_id_seq'::regclass);


--
-- Name: server_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_data_servers ALTER COLUMN server_id SET DEFAULT nextval('sign_data_servers_server_id_seq'::regclass);


--
-- Name: type_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_data_types ALTER COLUMN type_id SET DEFAULT nextval('sign_data_types_type_id_seq'::regclass);


--
-- Name: cat_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_def_cat ALTER COLUMN cat_id SET DEFAULT nextval('sign_def_cat_cat_id_seq'::regclass);


--
-- Name: op_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_def_op ALTER COLUMN op_id SET DEFAULT nextval('sign_def_op_op_id_seq'::regclass);


--
-- Name: op_category_index; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_def_op ALTER COLUMN op_category_index SET DEFAULT nextval('sign_def_op_op_category_index_seq'::regclass);


--
-- Name: event_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_event ALTER COLUMN event_pkey SET DEFAULT nextval('sign_event_event_id_seq'::regclass);


--
-- Name: file_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_files ALTER COLUMN file_pkey SET DEFAULT nextval('sign_files_file_id_seq'::regclass);


--
-- Name: group_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_groups ALTER COLUMN group_id SET DEFAULT nextval('sign_groups_group_id_seq'::regclass);


--
-- Name: layout_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_layout ALTER COLUMN layout_pkey SET DEFAULT nextval('sign_layout_layout_pkey_seq'::regclass);


--
-- Name: cache_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_layout_img_cache ALTER COLUMN cache_id SET DEFAULT nextval('sign_layout_img_cache_cache_id_seq'::regclass);


--
-- Name: layout_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_layouts ALTER COLUMN layout_id SET DEFAULT nextval('sign_layouts_layout_id_seq'::regclass);


--
-- Name: officehours_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_officehours ALTER COLUMN officehours_pkey SET DEFAULT nextval('sign_officehours_officehours_pkey_seq'::regclass);


--
-- Name: published_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published ALTER COLUMN published_id SET DEFAULT nextval('sign_published_published_id_seq'::regclass);


--
-- Name: type_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_region_types ALTER COLUMN type_id SET DEFAULT nextval('sign_region_types_type_id_seq'::regclass);


--
-- Name: region_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_regions ALTER COLUMN region_id SET DEFAULT nextval('sign_regions_region_id_seq'::regclass);


--
-- Name: rss_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_rss ALTER COLUMN rss_pkey SET DEFAULT nextval('sign_rss_rss_id_seq'::regclass);


--
-- Name: text_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_text ALTER COLUMN text_pkey SET DEFAULT nextval('sign_text_text_id_seq'::regclass);


--
-- Name: transition_id; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_transitions ALTER COLUMN transition_id SET DEFAULT nextval('sign_transitions_transition_id_seq'::regclass);


--
-- Name: twitter_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_twitter ALTER COLUMN twitter_pkey SET DEFAULT nextval('sign_twitter_twitter_pkey_seq'::regclass);


--
-- Name: weather_pkey; Type: DEFAULT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_weather ALTER COLUMN weather_pkey SET DEFAULT nextval('sign_weather_weather_pkey_seq'::regclass);


--
-- Name: sign_calendar_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_calendar
    ADD CONSTRAINT sign_calendar_pkey PRIMARY KEY (calendar_pkey);


--
-- Name: sign_cfg_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_cfg
    ADD CONSTRAINT sign_cfg_pkey PRIMARY KEY (cfg_desig);


--
-- Name: sign_commands_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_commands
    ADD CONSTRAINT sign_commands_pkey PRIMARY KEY (cmd_id);


--
-- Name: sign_data_servers_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_data_servers
    ADD CONSTRAINT sign_data_servers_pkey PRIMARY KEY (server_id);


--
-- Name: sign_data_types_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_data_types
    ADD CONSTRAINT sign_data_types_pkey PRIMARY KEY (type_id);


--
-- Name: sign_data_types_type_name_key; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_data_types
    ADD CONSTRAINT sign_data_types_type_name_key UNIQUE (type_name);


--
-- Name: sign_def_cat_cat_name_key; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_def_cat
    ADD CONSTRAINT sign_def_cat_cat_name_key UNIQUE (cat_name);


--
-- Name: sign_def_cat_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_def_cat
    ADD CONSTRAINT sign_def_cat_pkey PRIMARY KEY (cat_id);


--
-- Name: sign_def_op_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_def_op
    ADD CONSTRAINT sign_def_op_pkey PRIMARY KEY (op_id);


--
-- Name: sign_event_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_event
    ADD CONSTRAINT sign_event_pkey PRIMARY KEY (event_pkey);


--
-- Name: sign_files_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_files
    ADD CONSTRAINT sign_files_pkey PRIMARY KEY (file_pkey);


--
-- Name: sign_group_memberships_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_group_memberships
    ADD CONSTRAINT sign_group_memberships_pkey PRIMARY KEY (sign_desig);


--
-- Name: sign_groups_group_name_key; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_groups
    ADD CONSTRAINT sign_groups_group_name_key UNIQUE (group_name);


--
-- Name: sign_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_groups
    ADD CONSTRAINT sign_groups_pkey PRIMARY KEY (group_id);


--
-- Name: sign_layout_img_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_layout_img_cache
    ADD CONSTRAINT sign_layout_img_cache_pkey PRIMARY KEY (cache_id);


--
-- Name: sign_layout_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_layout
    ADD CONSTRAINT sign_layout_pkey PRIMARY KEY (layout_pkey);


--
-- Name: sign_layouts_layout_title_key; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_layouts
    ADD CONSTRAINT sign_layouts_layout_title_key UNIQUE (layout_title);


--
-- Name: sign_layouts_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_layouts
    ADD CONSTRAINT sign_layouts_pkey PRIMARY KEY (layout_id);


--
-- Name: sign_officehours_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_officehours
    ADD CONSTRAINT sign_officehours_pkey PRIMARY KEY (officehours_pkey);


--
-- Name: sign_published2group_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_published2group
    ADD CONSTRAINT sign_published2group_pkey PRIMARY KEY (group_id, published_id, region_id);


--
-- Name: sign_published2sign_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_published2sign_ex
    ADD CONSTRAINT sign_published2sign_pkey PRIMARY KEY (sign_designator, published_id, region_id);


--
-- Name: sign_published_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_published
    ADD CONSTRAINT sign_published_pkey PRIMARY KEY (published_id);


--
-- Name: sign_region_types_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_region_types
    ADD CONSTRAINT sign_region_types_pkey PRIMARY KEY (type_id);


--
-- Name: sign_regions_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_regions
    ADD CONSTRAINT sign_regions_pkey PRIMARY KEY (region_id);


--
-- Name: sign_regions_region_name_key; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_regions
    ADD CONSTRAINT sign_regions_region_name_key UNIQUE (region_name);


--
-- Name: sign_rss_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_rss
    ADD CONSTRAINT sign_rss_pkey PRIMARY KEY (rss_pkey);


--
-- Name: sign_text_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_text
    ADD CONSTRAINT sign_text_pkey PRIMARY KEY (text_pkey);


--
-- Name: sign_transitions_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_transitions
    ADD CONSTRAINT sign_transitions_pkey PRIMARY KEY (transition_id);


--
-- Name: sign_transitions_transition_name_key; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_transitions
    ADD CONSTRAINT sign_transitions_transition_name_key UNIQUE (transition_name);


--
-- Name: sign_twitter_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_twitter
    ADD CONSTRAINT sign_twitter_pkey PRIMARY KEY (twitter_pkey);


--
-- Name: sign_weather_pkey; Type: CONSTRAINT; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

ALTER TABLE ONLY sign_weather
    ADD CONSTRAINT sign_weather_pkey PRIMARY KEY (weather_pkey);


--
-- Name: sign_calendar_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_calendar_idx ON sign_calendar USING btree (calendar_published_id);


--
-- Name: sign_group_memberships_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_group_memberships_idx ON sign_group_memberships USING btree (sign_group_id);


--
-- Name: sign_layout_img_cache_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_layout_img_cache_idx ON sign_layout_img_cache USING btree (cache_layout_id, cache_user, cache_mtime);


--
-- Name: sign_published_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_published_idx ON sign_published USING btree (published_id, published_start, published_end);


--
-- Name: sign_published_idx1; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_published_idx1 ON sign_published USING btree (published_end, published_start);


--
-- Name: sign_rss_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_rss_idx ON sign_rss USING btree (rss_published_id);


--
-- Name: sign_text_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_text_idx ON sign_text USING btree (text_published_id);


--
-- Name: sign_twitter_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_twitter_idx ON sign_twitter USING btree (twitter_published_id);


--
-- Name: sign_weather_idx; Type: INDEX; Schema: public; Owner: cecsinet_dev; Tablespace: 
--

CREATE INDEX sign_weather_idx ON sign_weather USING btree (weather_published_id);


--
-- Name: sign_data_servers_lone_default_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_data_servers_lone_default_tr BEFORE INSERT OR UPDATE ON sign_data_servers FOR EACH ROW EXECUTE PROCEDURE sign_data_servers_lone_default_tr_fn();


--
-- Name: sign_group_memberships_asset_set_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_group_memberships_asset_set_tr BEFORE INSERT OR UPDATE OF sign_desig ON sign_group_memberships FOR EACH ROW EXECUTE PROCEDURE sign_group_memberships_asset_set_tr_fn();


--
-- Name: sign_group_memberships_check_config_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_group_memberships_check_config_tr AFTER INSERT OR UPDATE ON sign_group_memberships FOR EACH ROW EXECUTE PROCEDURE sign_group_memberships_check_config_tr_fn();


--
-- Name: sign_layout_img_cache_del_old_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_layout_img_cache_del_old_tr BEFORE INSERT ON sign_layout_img_cache FOR EACH ROW EXECUTE PROCEDURE sign_layout_img_cache_del_old_tr_fn();


--
-- Name: sign_layouts_invalidate_cache_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_layouts_invalidate_cache_tr AFTER INSERT OR DELETE OR UPDATE ON sign_layouts FOR EACH ROW EXECUTE PROCEDURE sign_layouts_invalidate_cache_tr_fn();


--
-- Name: sign_published_html_change_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_published_html_change_tr BEFORE UPDATE OF published_html ON sign_published FOR EACH ROW EXECUTE PROCEDURE sign_published_html_change_tr_fn();


--
-- Name: sign_region2layout_invalidate_cache_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_region2layout_invalidate_cache_tr AFTER INSERT OR DELETE OR UPDATE ON sign_region2layout FOR EACH ROW EXECUTE PROCEDURE sign_region2layout_invalidate_cache_tr_fn();


--
-- Name: sign_regions_invalidate_cache_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_regions_invalidate_cache_tr AFTER INSERT OR UPDATE ON sign_regions FOR EACH ROW EXECUTE PROCEDURE sign_regions_invalidate_cache_tr_fn();


--
-- Name: sign_text_update_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_text_update_tr BEFORE UPDATE OF text_value ON sign_text FOR EACH ROW EXECUTE PROCEDURE sign_text_update_tr_fn();


--
-- Name: sign_transitions_one_default_tr; Type: TRIGGER; Schema: public; Owner: cecsinet_dev
--

CREATE TRIGGER sign_transitions_one_default_tr AFTER UPDATE OF transition_default ON sign_transitions FOR EACH ROW EXECUTE PROCEDURE sign_transitions_one_default_tr_fn();


--
-- Name: sign_calendar_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_calendar
    ADD CONSTRAINT sign_calendar_fk FOREIGN KEY (calendar_published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_calendar_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_calendar
    ADD CONSTRAINT sign_calendar_fk1 FOREIGN KEY (calendar_id) REFERENCES ics_info(info_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_cfg_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_cfg
    ADD CONSTRAINT sign_cfg_fk FOREIGN KEY (cfg_dataserver) REFERENCES sign_data_servers(server_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_cfg_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_cfg
    ADD CONSTRAINT sign_cfg_fk1 FOREIGN KEY (cfg_content_schedule) REFERENCES ics_info(info_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_commands_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_commands
    ADD CONSTRAINT sign_commands_fk FOREIGN KEY (cmd_manu) REFERENCES inv_manufacturer(manu_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_commands_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_commands
    ADD CONSTRAINT sign_commands_fk1 FOREIGN KEY (cmd_category) REFERENCES sign_def_cat(cat_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_def_op_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_def_op
    ADD CONSTRAINT sign_def_op_fk FOREIGN KEY (op_category) REFERENCES sign_def_cat(cat_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_defaults_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_defaults
    ADD CONSTRAINT sign_defaults_fk FOREIGN KEY (sign_option) REFERENCES sign_def_op(op_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_event_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_event
    ADD CONSTRAINT sign_event_fk FOREIGN KEY (event_published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_files_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_files
    ADD CONSTRAINT sign_files_fk1 FOREIGN KEY (file_by) REFERENCES admin_user(user_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: sign_files_published_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_files
    ADD CONSTRAINT sign_files_published_id_fk FOREIGN KEY (file_published_id) REFERENCES sign_published(published_id);


--
-- Name: sign_group_defaults_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_group_defaults
    ADD CONSTRAINT sign_group_defaults_fk FOREIGN KEY (sign_group_id) REFERENCES sign_groups(group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_group_defaults_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_group_defaults
    ADD CONSTRAINT sign_group_defaults_fk1 FOREIGN KEY (sign_option) REFERENCES sign_def_op(op_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_group_memberships_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_group_memberships
    ADD CONSTRAINT sign_group_memberships_fk FOREIGN KEY (sign_group_id) REFERENCES sign_groups(group_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_group_memberships_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_group_memberships
    ADD CONSTRAINT sign_group_memberships_fk1 FOREIGN KEY (sign_asset) REFERENCES inv_asset_full(asset_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: sign_groups_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_groups
    ADD CONSTRAINT sign_groups_fk FOREIGN KEY (group_layout) REFERENCES sign_layouts(layout_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_groups_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_groups
    ADD CONSTRAINT sign_groups_fk1 FOREIGN KEY (group_priv) REFERENCES admin_privilege(priv_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_groups_fk2; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_groups
    ADD CONSTRAINT sign_groups_fk2 FOREIGN KEY (group_layout_alternate) REFERENCES sign_layouts(layout_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: sign_groups_fk3; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_groups
    ADD CONSTRAINT sign_groups_fk3 FOREIGN KEY (group_layout_fullscreen) REFERENCES sign_layouts(layout_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: sign_layout_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_layout
    ADD CONSTRAINT sign_layout_fk FOREIGN KEY (layout_published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_layout_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_layout
    ADD CONSTRAINT sign_layout_fk1 FOREIGN KEY (layout_target_layout) REFERENCES sign_layouts(layout_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_officehours_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_officehours
    ADD CONSTRAINT sign_officehours_fk FOREIGN KEY (officehours_published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_officehours_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_officehours
    ADD CONSTRAINT sign_officehours_fk1 FOREIGN KEY (officehours_calendar_id) REFERENCES ics_info(info_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_published2group_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published2group
    ADD CONSTRAINT sign_published2group_fk FOREIGN KEY (region_id) REFERENCES sign_regions(region_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_published2group_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published2group
    ADD CONSTRAINT sign_published2group_fk1 FOREIGN KEY (group_id) REFERENCES sign_groups(group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_published2group_published_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published2group
    ADD CONSTRAINT sign_published2group_published_id_fk FOREIGN KEY (published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_published2sign_ex_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published2sign_ex
    ADD CONSTRAINT sign_published2sign_ex_fk FOREIGN KEY (region_id) REFERENCES sign_regions(region_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_published2sign_ex_published_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published2sign_ex
    ADD CONSTRAINT sign_published2sign_ex_published_id_fk FOREIGN KEY (published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_published_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published
    ADD CONSTRAINT sign_published_fk FOREIGN KEY (published_type) REFERENCES sign_data_types(type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_published_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_published
    ADD CONSTRAINT sign_published_fk1 FOREIGN KEY (published_transition) REFERENCES sign_transitions(transition_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_region2layout_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_region2layout
    ADD CONSTRAINT sign_region2layout_fk FOREIGN KEY (inst_layout_id) REFERENCES sign_layouts(layout_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_region2layout_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_region2layout
    ADD CONSTRAINT sign_region2layout_fk1 FOREIGN KEY (inst_region_id) REFERENCES sign_regions(region_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sign_regions_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_regions
    ADD CONSTRAINT sign_regions_fk FOREIGN KEY (region_type) REFERENCES sign_region_types(type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_regions_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_regions
    ADD CONSTRAINT sign_regions_fk1 FOREIGN KEY (region_priv) REFERENCES admin_privilege(priv_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_regions_fk2; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_regions
    ADD CONSTRAINT sign_regions_fk2 FOREIGN KEY (region_backup) REFERENCES sign_regions(region_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: sign_text_fk1; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_text
    ADD CONSTRAINT sign_text_fk1 FOREIGN KEY (text_by) REFERENCES admin_user(user_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: sign_text_published_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cecsinet_dev
--

ALTER TABLE ONLY sign_text
    ADD CONSTRAINT sign_text_published_id_fk FOREIGN KEY (text_published_id) REFERENCES sign_published(published_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--


The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
