from sys import argv, exit
from os import fork, execv, waitpid

def main():
	cmd = argv[1]
	pid = 0

	while True:
		pid = fork()

		if pid == 0:
			execv(cmd, [''])
		else:
			pid, exit = waitpid(pid, 0)	

if __name__ == '__main__':
	if len(argv) < 2:
		print("No command")
		exit(1)
	main()
