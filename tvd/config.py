import json

class Config:
	def __init__(self):
		self.config_file = "config.json"
		self.Load()

	def Load(self):
		self.config = json.load(open(self.config_file, 'r'))
