from tv_interfaces import *
from exceptions import *
from config import Config
import re

class API:
	def __init__(self):
		self.config = Config()

	def GetTVManu(self):
		return self.config.config['tv']['manufacturer']

	def WriteOK(self, req, extra_headers = None):
		req.send_response(200)
		req.send_header('Content-Type', 'text/html')
		
		if extra_headers is not None:
			for k,v in extra_headers.items():
				req.send_header(k, v)

		req.end_headers()

	def WriteError(self, req, html = None, extra_headers = None):
		req.send_response(500)
		req.send_header('Content-Type', 'text/html')

		if extra_headers is not None:
			for k,v in extra_headers.items():
				req.send_header(k, v)

		req.end_headers()



	def WriteHTML(self, req, html, error = False):
		b = bytes(html, 'utf-8')
		if error:
			self.WriteError(req, {'Content-Length': len(b)})
		else:
			self.WriteOK(req, {'Content-Length': len(b)})

		req.wfile.write(b)
		
	def cmd2path(self, cmd):
		for setting in self.settings:
			if (setting in cmd) and cmd.index(setting) == 0:
				cmd = cmd[len(setting):].lower()

				if cmd == "query":
					return ("tv/%s" % setting)
				else:
					return ("tv/%s/%s" % (setting, cmd)) 
		return None

	def register_handlers(self, hdl):
		hdl.router['reload'] = self.settings_reload

		tvInterface = GetUsableInterface(self.GetTVManu())
		cmds = tvInterface.GetCommands()

		queryEx = re.compile('([a-zA-Z]+)[Qq]uery')

		self.settings = []

		for cmd in cmds:
			matches = queryEx.match(cmd)

			if matches is not None:
				setting = matches.group(1)
				self.settings.append(matches.group(1))

		for cmd in cmds:
			path = self.cmd2path(cmd)
			if path is not None:
				hdl.router[path] = self.router_generator(cmd)

		for path in hdl.router.keys():
			print("Established path: %s" % path)

		print("# Paths: %d, # Settings: %d" % (len(hdl.router), len(self.settings)))

	def settings_reload(self, req):
		self.config.Load()
		self.WriteHTML(req, "Config Reloaded")

	def router_generator(self, cmd):
		return lambda req : self.send_tv_cmd(cmd, req)

	def send_tv_cmd(self, cmd, req):
		IFace = GetUsableInterface(self.GetTVManu()) 
		try:
			if IFace.SendCommand(cmd):
				self.WriteOK(req)
			else:
				self.WriteError(req)

		except CommandNotFoundException as exc:
			self.WriteHTML(req, "Failed to find command (command = %s)" % exc, True)	

		except CommFailureException as exc:
			print("failure")
			self.WriteHTML(req, "Failure to communicate (command = %s)" % exc, True)	
