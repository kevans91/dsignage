from http.server import HTTPServer
from handler import api_handler
from api import API

def main():
	srv_addr = ('localhost', 8927)
	apiInst = API()
	apiInst.register_handlers(api_handler)	
	httpd = HTTPServer(srv_addr, api_handler)	
	print("Listening on localhost:8927")
	httpd.serve_forever()
	
if __name__ == '__main__':
	main()
