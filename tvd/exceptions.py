class CommandNotFoundException(Exception):
	pass

class CommFailureException(Exception):
	pass

class SerialFailureException(CommFailureException):
	pass

class EthernetFailureException(CommFailureException):
	pass
