import json
import serial
from config import Config
from exceptions import CommandNotFoundException, SerialFailureException, EthernetFailureException

# Mapping from config file values to actual interface names
Interfaces = {
	'serial': 'SerialInterface',
	'ethernet': 'EthernetInterface',
}

class TVInterface:
	def __init__(self, manu = 'LG'):
		self.manu = manu	
		self.cmdList = None
		self.config = Config()

	def GetCommands(self):
		if self.cmdList is None:
			tvList = json.load(open('commands.json', 'r'))
			self.cmdList = tvList[self.manu]['commands']
			
		return self.cmdList

	def FindCommand(self, name):
		cmdList = self.GetCommands()

		if name in cmdList:
			return cmdList[name]
		else:
			return None
	
	def SendCommand(self, name):
		return NotImplemented

	def GetStatus(self):
		return NotImplemented


class SerialInterface(TVInterface):
	def GetRate(self):
		tvconf = self.config.config['tv']
		return tvconf['rate']

	def GetPort(self):
		tvconf = self.config.config['tv']
		return tvconf['port']

	def SendCommand(self, name):
		cmd = self.FindCommand(name)

		if cmd is None:
			raise CommandNotFoundException(name)

		try:
			# Send serial command
			with serial.Serial(self.GetPort(), self.GetRate(), timeout=1) as ser:
				ser.write(bytes(cmd, 'utf-8'))
		except:
			raise SerialFailureException(name)

		return True

	def GetStatus(self):
		return 0

class EthernetInterface(TVInterface):
	def SendCommand(self, name):
		cmd = self.FindCommand(name)

		if cmd is None:
			raise CommandNotFoundException(name)

		print(cmd)

			# TODO: Send `cmd` over ethernet
		return False

	def GetStatus(self):
		return 0

	# NOTE: 'manu' is actually a manufacturer
def GetUsableInterface(manu):
	tvList = json.load(open('commands.json', 'r'))
	if manu in tvList:
		interface = tvList[manu]['interface']

		if interface in Interfaces:
			return globals()[Interfaces[interface]](manu)
		else:
			return None		
	else:
		return None
