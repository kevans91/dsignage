from http.server import BaseHTTPRequestHandler

class api_handler(BaseHTTPRequestHandler):
	router = {}

	def do_GET(self):
		if self.path[0] == '/':
			self.path = self.path[1:]

		for k, v in self.router.items():
			if k == self.path:
				return v(self)

		self.send_response(404, 'Not Found')
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(bytes("""<html>
						<head><title>404: Not Found</title></html>
						<body>
							Whoops, wrong car
						</body>
					</html>""", 'utf-8'))

