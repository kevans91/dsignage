/**
	sign-basic.js by Kyle Evans
	
	Purpose: Basic functionality of the sign client, independent of data
		-- Controls refresh behavior, mainly, and some other global behavior
**/

function inIFrame() {
	/* Blatantly stolen from Greg on StackOverflow: https://stackoverflow.com/questions/326069/how-to-identify-if-a-webpage-is-being-loaded-inside-an-iframe-or-directly-into-t */
	try {
		return window.self !== window.top;
	} catch (e) {
		return true;
	}
}

function idify(name) {
	if(!name || (typeof(name) === 'undefined') || name.length === 0) return name;
	
	return name.toLowerCase().replace(/ /g, '-').replace(/[^A-Za-z\-0-9]/g, '');
}

function getSignURL(desig) {
	return (window.location.protocol + '//' + window.location.hostname + window.location.pathname + '?sign=' + desig + '#' + desig);
}

	/* Time in seconds */
function Now() {
	return Math.floor((new Date).getTime()/1000);;
}

	/* Now(), except adjusted to server time */
function adjNow() {
	var ofs = 0;
	if((typeof window.dateTimeOffset) != 'undefined')
		ofs = window.dateTimeOffset;
	
	return (Now() + ofs);
}

	/* allowedAge should be in minutes */
function Refreshable(scriptName, cacheName, onReceiveData, allowedAge, dataServer) {
	this.dataServer = (typeof dataServer !== 'undefined' ? dataServer : window.cfg.dataServer);

	$(this).on('cron', function() {
			/* We'll go ahead and do the update if we're within 15 seconds */
		if(this.willRun(window.cfg.currentTick))
			$(this).trigger('refresh');		
		
		return true;
	});

	this.cacheKey = function() {
		var evt = $.Event('alter-cache-key');
		evt.key = (this.cacheName + "__default__" + encodeURIComponent(window.location.href));
		
		$(this).trigger(evt);
		return evt.key;
	}
	
	this.getCacheName = function() {
		return this.cacheKey();
	}
	
	this.calculateRemote = function() {
		if(this.dataServer != '')
			this.remote = this.dataServer + dataScriptsDir + "/" + this.scriptName;
		else
			this.remote = '';
	}
	
	this.onRemoteChange = function() {
		this.calculateRemote();
		this.forceRefresh();
	}
	
	this.coldRefresh = function() {
			/* Cold refreshes do not need to happen if we are currently refreshing */
		if(!this.willRun(window.cfg.currentTick)) {
			data = window.localStorage.getItem(this.getCacheName());
			if(data)
				this.onReceiveData($.parseJSON(data));
		}
	}
	
	this.forceRefresh = function() {
		var _this = this;
		
		if(this.refreshLock)	/* Keep looping this until lock is over */
			setTimeout(function() { _this.forceRefresh() }, 1000);
		
		$(this).trigger('refresh');
	}

	
	this._failedRefresh = function() {
		var data = window.localStorage.getItem(this.getCacheName());
		++ this.refreshFails;
		this.onReceiveData($.parseJSON(data));

		if(this.postReceiveData)
			this.postReceiveData();

		this.refreshLock = 0;
		this.refreshing = false;
	};

	this.isForced = function() {
		return false;
	}
	
	this.willRun = function(tick) {
		return (this.isForced() || (tick % this.allowedAge) == 0);
	}
	
	$(this).on('refresh', function() {
		var designator = (window.labDesignator !== "dev-dev" ? window.labDesignator : "sign-dev");
		var requestUrl = this.remote + "?sign=" + designator;
		
		var src = this;
				
		if(src != window.cfg.refreshable && !window.cfg.loaded)
			return false;
		
		if(this.refreshLock == 0) {
			this.refreshLock = 1;
			this.refreshing = true;
			var $refreshable = $(this);
			
			if(this.remote == '') {
				this._failedRefresh();
				return;
			}
		
			if(window.preview || inIFrame())
				requestUrl += "&preview=true";

			var evt = $.Event('alter-request');
			evt.request = requestUrl;
			
			$refreshable.trigger(evt);
			$refreshable.trigger('before-refresh');
				
			var jqXHR = $.get(evt.request, function(rawData) {			

				try {
					data = $.parseJSON(rawData);
					window.localStorage.setItem(src.getCacheName(), rawData);

					src.refreshFails = 0;

					src.onReceiveData(data);

					src.refreshLock = 0;
					src.refreshing = false;

					if(src.postReceiveData)
						src.postReceiveData();					
					
					$refreshable.trigger('after-refresh', [true]);
				}
				
				catch (e) {
						/* We tried, and failed... pull from cache */
					src._failedRefresh();
					$refreshable.trigger('after-refresh', [false]);
				}
			});
			
			jqXHR.fail(function() {
				src._failedRefresh();
				$refreshable.trigger('after-refresh', [false]);
			});
		}			
	});
	
	this.scriptName = scriptName;
	this.remote = '';
	this.cacheName = cacheName;
	this.onReceiveData = onReceiveData;
	this.allowedAge = allowedAge;
	
	this.refreshable = true;
	this.refreshFails = 0;
	this.refreshLock = 0;
	
		/* Status stuff */
	this.refreshing = false;
	this.pending = false;
	this.building = false;
	
	if(typeof window.refreshables == "undefined")
		window.refreshables = [];
	
	this.calculateRemote();
	window.refreshables.push(this);
}

$(document).on('cron', function() {
	window.cronStart = adjNow();
	
	var deferred = window.cfg.deferRefreshes;
	
	if(!deferred) {		
		window.cfg.currentTick = (window.cfg.currentTick + 1) % window.cfg.wrapTick;

		$.each(window.refreshables, function(idx, e) {
			$(e).trigger('cron');
		});
	}
	
		/* Try again in a second if any loads were deferred */
		/* This will throw off the rest of our timing, but that's OK */
	setTimeout(function() {
		$(document).trigger('cron');
	}, (deferred ? 1000 : 60000));
});

$(document).ready(function() {
	console._log = console.log;
	
		/* console.log is a no-op under our standard design */
	console.log = function(s) {};
	
	setTimeout(function() {
		$(document).trigger('cron');
	}, 60000);
});
