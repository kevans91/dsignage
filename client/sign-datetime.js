/**
	sign-datetime.js by Kyle Evans
	
	Purpose: Synchronizing local datetime with remote datetime -- remote is authorizative
**/

window.getDate = function() {
	var dtime = moment();
	dtime.unix(Now() + window.dateTimeOffset);
	
	return dtime.format(window.cfg.dateFormat);	
}

window.getTime = function() {
	var dtime = moment();
	dtime.unix(Now() + window.dateTimeOffset);
	
	return dtime.format(window.cfg.timeFormat);	
}

window.getDateTime = function() {
	var dtime = moment();
	dtime.unix(Now() + window.dateTimeOffset);
	
	return "<span class='datetime'>" +
			"<span class='time'>" + dtime.format(window.cfg.timeFormat) + "</span>" +
			"<span class='day'>" + dtime.format(window.cfg.dayFormat) + "</span>" +
			"<span class='date'>" + dtime.format(window.cfg.dateFormat) + "</span></span>";
}

window.datetimeReceived = function(data) {
	var dtime = data;
	
		/* dateTimeOffset is to be added to our local time when updating date time regions */
	window.dateTimeOffset = (dtime - Now());
};

$(document).on('date-tick', function() {
	var changed = false;
	
	$(".region.type-date, .region.type-time, .region.type-datetime").each(function() {
		changed = true;

		if(window.region && ($(this).data('region-folder') != idify(window.region)))
			return;

		if($(this).hasClass('type-date'))
			$(this).children(".container").text(window.getDate());
		else if($(this).hasClass('type-time'))
			$(this).children(".container").text(window.getTime());
		else if($(this).hasClass('type-datetime'))
			$(this).children(".container").html(window.getDateTime());
	});

	setTimeout(function() {
		$(document).trigger('date-tick');
	}, (!changed ? 500 : 5000));
});

$(document).ready(function() {
	window.dateTimeOffset = 0;
	window.datetimeSync = new Refreshable("datetime.php", "datetime-cache", window.datetimeReceived, window.cfg.datetimeAllowedAge);

	$(document).trigger('date-tick');
});
