var TOP_CONST = 1;
var RIGHT_CONST = 2;
var BOTTOM_CONST = 4;
var LEFT_CONST = 8;

$.fn.cycle.transitions.pixelate = {
		before: function(opts, curr, next, fwd) {
			opts.API.stackSlides(next, curr, fwd);
			opts.cssBefore = { opacity: 1, visibility: 'visible', display: 'block', "-webkit-transform": "rotate(180deg)" };
			opts.cssAfter = { opacity: 0, visibility: 'hidden', display: 'none', "-webkit-transform": "rotate(180deg)" };
		},
		
		transition: function(opts, cur, next, ff, callback) {
			var $cur = $(cur);
			var $next = $(next);
			
			var width = $next.width();
			var height = $next.height();
			
			$next.css(opts.cssBefore || {});
			$cur.css(opts.cssAfter || {});

			setTimeout(callback, opts.speed);			
		},
};

$.fn.cycle.transitions.scrollHorzDir = {
	before: function( opts, curr, next, fwd ) {
		opts.API.stackSlides( curr, next, fwd );
		var w = opts.container.css('overflow','hidden').width();

		if(typeof opts.direction == 'undefined')
			opts.direction = -1;
		
		opts.cssBefore = { left: (-opts.direction * w), top: 0, opacity: 1, visibility: 'visible', display: 'block' };
		opts.cssAfter = { zIndex: opts._maxZ - 2, left: 0 };

		opts.animIn = { left: 0 };
		opts.animOut = { left: (opts.direction * w) };
	}
};

$.fn.cycle.transitions.scrollVertDir = {
	before: function( opts, curr, next, fwd ) {
		opts.API.stackSlides( curr, next, fwd );
		var h = opts.container.css('overflow','hidden').height();

		if(typeof opts.direction == 'undefined')
			opts.direction = -1;
		
		opts.cssBefore = { left: 0, top: (-opts.direction * h), opacity: 1, visibility: 'visible', display: 'block' };
		opts.cssAfter = { zIndex: opts._maxZ - 2, left: 0, top: 0 };

		opts.animIn = { top: 0 };
		opts.animOut = { top: (opts.direction * h) };
	}
};

$.fn.cycle.transitions.fly = {
	before: function( opts, curr, next, fwd ) {
		opts.API.stackSlides( curr, next, fwd );
		var $next = $(next);
		var $curr = $(curr);

		var w = opts.container.css('overflow','hidden').width();
		var h = opts.container.css('overflow','hidden').height();
		
		var _h = $curr.height();
		var _w = $curr.width();
		/*
			var TOP_CONST = 1;
			var RIGHT_CONST = 2;
			var BOTTOM_CONST = 4;
			var LEFT_CONST = 8;
		*/
		
		if(typeof opts.origin == 'undefined')
			opts.origin = TOP_CONST | RIGHT_CONST;
		
		opts.cssBefore = { left: ((opts.origin & RIGHT_CONST) ? w : -w), top: ((opts.origin & TOP_CONST) ? -h : h), opacity: 1, visibility: 'visible', display: 'block', width: '0px', height: '0px'};
		opts.cssAfter = { zIndex: opts._maxZ - 2, left: 0, top: 0, width: _w + 'px', height: _h + 'px' };

		opts.speed *= 6;
		
		opts.animIn = { left: 0, top: 0, width: w + 'px', height: h + 'px' };
		opts.animOut = { left: ((opts.origin & RIGHT_CONST) ? -w : w), top: ((opts.origin & TOP_CONST) ? -h : h)};
	},
};