/**
	sign-marquee.js by Kyle Evans
	
	Purpose: Creating the functionality of a marquee
**/

	/* Seconds to get across the region */
var marquee_scroll_rate = 8000;

function doHide($elem) {
	$elem.css('visibility', 'hidden');
}

function doShow($elem) {
	$elem.css('visibility', 'visible');
}

$.fn.marquee = function(opts) {
	var $this = $(this);
	this.opts = opts;
	
	if($this.find('section').length == 0) {
		console.log('[marquee] Not starting empty marquee');
		return false;
	}
	
	console.log('[marquee] Starting');	
	
	$this.find('section').each(function() {
		var $_this = $(this);
		$_this.parent().replaceWith($_this);
	});
	
		/* We only support content that gets wrapped in <section> here */
	$this.children(':not(section)').not(".clearfix").remove();
	doHide($this.children().not(".clearfix"));
	doShow($this);
	
		/* On content refreshes, we re-use the container that the marquee is created on */
		/* To combat this, we remove all data and remove previous marquee-specific event handlers */
	$this.off('marquee-start marquee-transition');
	$this.removeData();
	
		/* Setup event handlers */
	$this.on('marquee-start', function(evt) {
		var $this = $(this);
		var vis = $this.is(':visible');
		
		doHide($this.find('article, section'));
		console.log('[marquee] marquee-start');
	
		if(!vis) {
			console.log('[marquee] Hidden, not looping further');
			return ;
		} else {
			$this.trigger('marquee-transition');
		}
	});
	
	$this.on('marquee-transition', function(evt) {
			/* Welcome to our marquee loop */
			/* We marquee things */
			/* We marquee all of the things */		
		var $this = $(this);
		var $parent = $this;
		
		var $currSect = $this.data('current-section');
		var $currArticle = $this.data('current-article');
		var initSection = false;
		var reInit = false;
		
		console.log('[marquee] marquee-transition');

		if(typeof $currSect === 'undefined') {
			console.log("[marquee] no current section");
			$currSect = $this.children('section').first();		
			$currArticle = $currSect.find('article').first();
			$this.data('marquee-iter-start', Now());

			initSection = true;
		} else {
			console.log("[marquee] cycling article");
			$currArticle = $currArticle.next('article');
			
				/* If there is no current article, transition to the next section */
			if($currArticle.length == 0) {
				console.log("[marquee] cycling section");
				reInit = initSection = true;
				
					/* Clean up previous section, determine new current section */
				doHide($currSect);
				$currSect = $currSect.next('section');
				
					/* Wrap around to the first <section> if we've hit the end */
				if($currSect.length == 0) {
					console.log("[marquee] section wrap");
					$currSect = $this.children('section').first();
				}
				
					/* Finally, determining new current article */
				$currArticle = $currSect.find('article').first();
			}
		}
				
		if(initSection || reInit)
			$this.data('marquee-iter-start', Now());

				/* Save the current section/article that we've determined */
		$this.data('current-section', $currSect);
		$this.data('current-article', $currArticle);
		
		var marqueeId = 'marquee' + $this.closest(".region").prop("id") + $this.data('marquee-iter-start');
		
			/* initSection means we need to determine all of our animations */
		if(initSection) {			
			dAssert($currSect.length == 1);

			doHide($currSect.find('article, h1'));
			doShow($currSect);

			var $container = $this.closest(".container");
			var containerWidth = $container.width();
			var scaleRt = (marquee_scroll_rate / containerWidth);
						
			var $articles = $currSect.find('article');
			
			$articles.each(function() {
				var $this = $(this);

				var thisWidth = $this.get(0).scrollWidth;

				$this.css('position', 'absolute');
				$this.css('right', '-' + thisWidth + 'px');

				var duration = marquee_scroll_rate + (scaleRt * thisWidth);
				
				$this.animate({
						right: containerWidth + 'px',
					},
					{
						duration: duration,
						queue: marqueeId,
						easing: 'linear',
						complete: function() {
								/* If this isn't :visible at this point, it must have disappeared */
							if($this.is(":visible")) {
								doHide($this);
								console.log('[marquee] animation complete');
								$parent.trigger('marquee-transition');
							}
						},
					}
				);
			});
		}
		
		dAssert($currArticle.length == 1);
		
		if(reInit)
			dAssert($currArticle.is(":first-of-type"));
			
		doShow($currArticle);
		$currArticle.dequeue(marqueeId);
	});
	
		/* Start the marquee */
	$this.trigger('marquee-start');
};