
var usingDevServer = 0;

var signBaseDir = '../';

var imgPath = signBaseDir + 'images/';
var bgPath = imgPath + 'bg/';
var origPath = imgPath + 'orig/';
var regionsPath = imgPath + 'regions/';
var genericPath = imgPath + 'generic/';
var tmpPath = imgPath + 'tmp/';

var dataScriptsDir = "/signs";

function Cfg() {
	this.loaded = false;
	
		/* We're probably always going to pull configuration from production intranet */
	this.source = 'https://intranet.engg.ksu.edu';
	this.dataServer = 'https://intranet.engg.ksu.edu'; 
	//this.dataServer = 'https://dev-kevans91-s.engg.ksu.edu';

	this.serverWarmupWaitTime = 2.5;		// 1 second
	this.serverHotWaitTime = 5;				// 5 seconds
	
	this.wordsPerSecond = 3;				// Average reading speed between 250 to 300

	this.layoutAllowedAge = 15;				// 15 ticks
	this.contentAllowedAge = 10;			// 5  ticks
	this.datetimeAllowedAge = 5;			// 5  ticks
	this.currentTick = 0;
	this.wrapTick = 60;
	this.animFPS = 30;
	
	this.cycleHacksEnabled = true;
	
	this.dateFormat = "MMMM D, YYYY";
	this.timeFormat = "h:mm A";
	this.dayFormat = "dddd";
	this.group = -1;
	this.reportData = false;

	this.force_content = false;
	this.force_layout = false;
	
	this.reloadHandler = function(value) {
		if(value)
			window.location.reload();
	};
	
	this.fields_pseudo_handlers = {
		"reload": this.reloadHandler,
	};
	
	this.fields_remote2local = {
		"layoutallowedage": "layoutAllowedAge",
		"dataserver": "dataServer",
	};
	
	this.fieldChanged = function(field, value) {
			/* Look-up this field in the remote2local map, if available */
		if(field in this.fields_remote2local)
			field = this.fields_remote2local[field];

			/* Does this field exist? No? OK- no change */
		if(typeof (this[field]) == 'undefined' && !(field in this.fields_pseudo_handlers))
			return;	

		if(field in this.fields_pseudo_handlers) {
			this.fields_pseudo_handlers[field](value);
		} else {
				/* Save the old value, reset it */
			var oldValue = this[field];
			this[field] = value;

			switch(field) {
					case 'dataServer': {
						for(var idx in window.refreshables) {
							var refreshable = window.refreshables[idx];

							if(refreshable != window.cfg.refreshable && refreshable.dataServer != value) {
								refreshable.dataServer = value;
								refreshable.onRemoteChange();
							}
						}
						break;
					}
					
					case 'group': {
							/* If the group changes, we need to force all refreshables to refresh */
							/* Essentially: reset the sign to tick 0 */
						this[field] = parseInt(this[field]);

						if(oldValue != value && oldValue >= 0)
							window.cfg.currentTick = (window.cfg.wrapTick - 1);

						break;
					}
			}
		}
	};
	
	this.newConfig = function(config) {
		var wasLoaded = window.cfg.loaded;
		window.cfg.loaded = true;

		for(var field in config) {
			window.cfg.fieldChanged(field, config[field]);
		}
		
		if(!wasLoaded) {
			for(var idx in window.refreshables) {
				var refreshable = window.refreshables[idx];
				if(refreshable != window.cfg.refreshable)
					$(refreshable).trigger('refresh');
			}
		}
	};		
}

window.cfg = new Cfg();

$(document).ready(function() {
	window.signStart = adjNow();
	$.fx.interval = (1000.0 / window.cfg.animFPS);
	
	if(typeof window.labDesignator === 'undefined') {
		labDesignator = window.location.hash;
		if(labDesignator.length === 0)
			labDesignator = "#sign-dev";

		window.labDesignator = labDesignator.substr(1).toLowerCase();
	}

	if(isDevLocal())
		$('body').append($("<div style='position: absolute; height: 10px; width: 100%; top: 0px; left: 0px; background-color: #FF0000;'></div>"));

	window.cfg.refreshable = new Refreshable('cfg.php', 'cfg-cache', window.cfg.newConfig, 1, window.cfg.source);

	$(window.cfg.refreshable).on('alter-request', function(evt) {
			if(window.cfg.reportData) {
				var doCollect = ["innerHeight", "innerWidth", "outerHeight", "outerWidth", "screenLeft", "screenTop", "screenX", "screenY"];
				var data = {};
				
				for(var idx in doCollect) {
					attr = doCollect[idx];
					
					data[attr]  = window[attr];
				}
				
				evt.request += "&data=" + encodeURIComponent(JSON.stringify(data));
			}
	});
	
	window.cfg.refreshable.onRemoteChange();
	
	if(window.localStorage.length > 0) {
			/* We timeout faster if we already have anything in our cache */
			/* We REALLLLY want to get something in the cache */
		$.ajaxSetup({timeout: window.cfg.serverWarmupWaitTime * 1000});
	
			/* Revert to hot wait time after a minute */
			/* We're OKAY with waiting a little bit longer if we have something displaying */
		setTimeout(function() {
			$.ajaxSetup({timeout: window.cfg.serverHotWaitTime * 1000});
		}, 60000);
	} else {
		$.ajaxSetup({timeout: window.cfg.serverHotWaitTime * 1000});
	}
});
