/**
	sign-dev.js by Kyle Evans
	
	Purpose: Any and all development related functionality
**/

function isDev() {
	return (labDesignator === "dev-dev");
}

function isKindaDev() {
	return (labDesignator === "sign-dev" || labDesignator === "sign-cfg" || isDev());
}

function dAlert(s) {
	if(isDev())
		alert(s);
}

function dLog(s) {
	if(isDev())
		console._log(s);
}

function dAssert(expr) {
	if(isDev() && !expr)
		throw "ASSERTION ERROR";
}

function isDevLocal() {
	var path = window.location.pathname.split('/');
	var localPath = path[path.length - 2];

	return (localPath === 'local-dev');
}

function devLocalToggle() {
	if(labDesignator === 'sign-dev') {
		if(isDevLocal()) {
			window.location = '../local/index.html#sign-dev';
		} else {
			window.location = '../local-dev/index.html#sign-dev';
		}
	}
}

$(document).ready(function() {
	if(isDev())
		console.log = dLog;
});