<?php

$sessKey = $_SERVER['REQUEST_URI'];
session_name($sessKey);
session_start();

$pullVars = array('sign', 'region', 'fg', 'bg');
$colorVars = array('fg', 'bg');
/*
if(!isset($_SESSION[$sessKey]) || !is_array($_SESSION[$sessKey]))
	$_SESSION[$sessKey] = array();

$sessVars =& $_SESSION[$sessKey];
*/
$sessVars =& $_SESSION;

foreach($pullVars as $var) {
	if(isset($_GET[$var]))
		$sessVars[$var] = $_GET[$var];
	else if(isset($sessVars[$var]))
		unset($sessVars[$var]);
}

foreach($colorVars as $var) {
	if(isset($sessVars[$var]) && $var[0] != '#')
		$sessVars[$var] = '#' . $sessVars[$var];
}

ob_start();

require_once('./index.html');

$idx = ob_get_contents();
ob_end_clean();

echo $idx;
