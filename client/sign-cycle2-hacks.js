/**
	sign-cycle2-hacks.js by Kyle Evans
	
	Purpose: Doing sketchy things to get jQuery cycle2 to function the way we'd like
		-- Allow a slide to be skipped from cycle-before, in case a video is not ready
**/


	/* myCalcNextSlide and myUpdateView used to replace jQuery cycle's */
	/* They both call the version they replaced, if applicable */
window.myCalcNextSlide = function() {
	var opts = this.opts();

	/* Interrupt the calculation */
	if(opts.skipping === true) {
		opts.API.log('Skipping calcNextSlide() ...');
		opts.nextSlide = ((opts.nextSlide + 1) % opts.slideCount);		
		return;
	}

		/* Call the original version, otherwise */
	opts.API._calcNextSlide();
};

window.myUpdateView = function(after, during, force) {
	var opts = this.opts();
	
		/* Interrupt the calculation */
	if(opts.skipping === true) {
			/* We're the last of the chain of execution that we're killing off */
			/* Reset skipped */
		opts.API.log('Skipping updateView() ...');
		opts.skipping = false;
		opts.busy = false;
		opts.API.prepareTx(false, true);
		return;
	}
	
		/* Call the original version, otherwise */
	opts.API._updateView(after, during, force);
};

$(document).ready(function() {
		/* And thus begins the tale of how I did sketchy */
		/* It was a dark and stormy (a.k.a totally clear) afternoon */
		/* jQuery Cycle 2 wouldn't let me just 'skip' a slide from cycle-before... it had to be removed */
		/* So I dug, and I dug, and I dug. */
		/* Then, I found the answer: weird magic! */
		/* I kicked into motion a chain of events that I could not stop */
		/* For each transition, we have to override the transition and before functions */
		/* This stops the transition from happening, effectively- the transition function */
		/* Is supposed to call 'callback', but we simply don't and let execution crawl back up */
		/* through API.calcNextSlide() and API.updateView() */
		/* In API.calcNextSlide()'s override (further up), we redo opts.nextSlide so that it skips one */
		/* In API.updateView(), it comes full-spin and we reset the skip flag and call prepareTx */
		/* The one that started it all. */
		/* And thus, ends my tale, weary traveller */
		/* I bid you, adeiu */
	if(!window.cfg.cycleHacksEnabled)
		return;
	
	$.each($.fn.cycle.transitions, function(idx, def) {
		if(typeof def.transition !== 'undefined')
			def._transition = def.transition;
		
		def.transition = function(opts, curr, next, fwd, callback) {
			var showOpts = opts.API.opts();

				/* Return to original chain of execution */
				/* Our overriden updateView() will calll prepareTx as it finishes */
			if(showOpts.skipping === true) {
				showOpts.API.log('Skipping transition() ...');
				return;
			}
			
			if((typeof this._transition) !== 'function')
				opts.API.doTransition(opts, curr, next, fwd, callback);
			else
				this._transition(opts, curr, next, fwd, callback);
		};
		
		if(typeof def.before !== 'undefined')
			def._before = def.before;
		
		def.before = function(opts, cur, next, fwd) {
			if(opts.skip === true) {
				opts.skip = false;
				
				opts = opts.API.opts();
				opts.skipping = true;
				
				opts.API.log('Skipping before() ...');
				return;
			}
			
			if(typeof this._before === 'function')
				this._before(opts, cur, next, fwd);
		};
	});
});