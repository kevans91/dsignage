/**
	sign-content.js by Kyle Evans
	
	Purpose: All content-related functionality of the sign
**/

function countWords(s) {
	return s.replace('/', ' ').replace(':', ' ').split(/\s+/).length;
}

window.contentLoadError = function($this) {
	var $localSlide = $this.closest(".slide:visible");

	if($localSlide.hasClass('cycle-slide')) {
			/* Use $localSlide.parent() for this, so that we don't make assumptions about the structure */
			/* This setup may change later */
		var idx = $localSlide.parent().children(".cycle-slide:not(.cycle-sentinel)").index($localSlide);
		
		var $show = $localSlide.closest('.cycle-slideshow');
		$show.cycle('remove', idx);
	} else {
			/* If it's not setup as a slideshow, or it's something else, just remove the container */
		$localSlide.remove();
	}
};

window.contentReceived = function(data) {
		/* Take pause until we know that the layout's done */
	if(window.layout && (window.layout.refreshing || window.layout.building)) {
		window.content.pending = true;
		setTimeout(function() {
			window.contentReceived(data);
		}, 100);
		
		return;
	}

	window.content.pending = false;
	window.content.building = true;
	
	$(".slideshow-container.cycle-slideshow").cycle("destroy")
	$(".region").find(":not(.container)").remove();
		
	var videoShows = [];
	
	$.each(data, function(idx, info) {
		var idified = idify(info['region_name']);
		
		if(window.region && idified != idify(window.region))
			return true;

		var tgt = "#region-" + idified;
		var $region = $(tgt);
		var $container = $region;
		var srcPath = regionsPath + idified + "/";
		
			/* Region handlers will add types to this as necessary */
		var $content = $("<div class='content'></div>");
		$content.append($(info['html']));
		
			/* If the region has children CSS to apply, apply it here */
		if((typeof $region.data('child-css')) !== 'undefined')
			$content.attr('style', $region.data('child-css'));
		
		if($region.children('.container').length != 0)
			$container = $region.children('.container');
		
		if($region.hasClass('type-slideshow')) {
			var $slideshow = $container;		
			
			$content.addClass('slide');
			$content.css('line-height', $region.height() + 'px');
			$content.find('section, article').css('line-height', $region.css('line-height'));
			
			var slideHasVideo = ($content.find('video').length > 0);
			var slideHasText = ($content.find('article').length > 0) && !($content.children('section').hasClass('weather-feed'));
		
			if(slideHasVideo) {
				$content.data('contains-video', true);
				$container.data('contains-video', true);
				
				if(videoShows.indexOf($container) < 0)
					videoShows.push($container);
			} else if(slideHasText) {		
				var txt = $content.text();
				var timeout = countWords(txt) / window.cfg.wordsPerSecond;
				$content.data('cycle-timeout', timeout * 1000);
			} else {	/* Timeout ignored for slides containing videos or text */
				$content.data('cycle-timeout', parseInt(info['timeout']) * 1000);
			}
		
			if($content.children('section').hasClass('weather-feed')) {
				var region = $region.get(0);
				var isTall = (region.clientHeight > region.clientWidth);

				if(isTall) {
					$content.find('article:not(:first-child)').each(function() {
						$(this).children('p.metar-info').remove();
						$(this).css('padding-top', '15px');
					});
					
					$content.find('p:not(.metar-info)').css('font-size', '22px').css('line-height', '18px');
				}
			}
		
			if(info['fx_func'])
				$content.data('cycle-fx', info['fx_func']);
			
			if(info['fx_args']) {
				args = JSON.parse(info['fx_args']);
				for(var key in args) {
					var val = args[key];
					$content.data('cycle-' + key, val);
				}
			}
		}
		
		if(info['is_backup'] == 't') {
			$content.addClass("backup");
		} else {
			$content.addClass("not-backup");
		}
			
		var $iframes = $content.find('iframe');
		$iframes.width($region.width());
		$iframes.height($region.height());
			
	
			/* Images and video <source>s need this */
		$content.find('.sign-local').each(function() {
			var $this = $(this);
			var src = $this.attr('src');
			
				/* Generic path is used for weather icons and other non-specific icons */
				/* The distinction matters, because these we have no intention of scaling */
			if($this.hasClass("sign-generic"))
				$this.attr('src', genericPath + src);
			else
				$this.attr('src', srcPath + src);
		});

			/* I don't know why I want to do this yet, but I'm sure I'll come up with a good reason */
		$content.find('img:not(.sign-local), video > source:not(.sign-local)').addClass('sign-remote');

		$content.find('img.sign-image').on('load', function() {
			if($(this).height() < $region.height())
				$(this).css('vertical-align', 'middle');
		});
		
		$content.find('img').on('error', function() {
			window.contentLoadError($(this));
		});
		
		$content.find('video').each(function() {
				/* Register some helpful callbacks */
			var $video = $(this);

			$video.on('canplaythrough', function() {
				$(this).data('ready', true);
			});
			
			$video.on('suspend', function() {
				var $this = $(this);

					/* If we move from a ready state to a non-ready state via suspend, attempt to reload */
				if($this.data('ready') === true) {
					$this.data('ready', false);
					$this.get(0).load();
				} else {
					$this.data('ready', false);
				}
			});
							
			$video.on('error abort emptied', function() {
				var $this = $(this);
				
					/* If we encounter an error on a previously OK video, let's attempt a recover by reload */
					/* Otherwise, signal a load error to remove the slide */
				if($this.data('ready') === true) {
						/* Reset the ready state so that the slide gets skipped in the meantime */
						/* Also, if it fails to load a second time, we'll end up falling back to a load error */
					$(this).data('ready', false);
					$(this).get(0).load();
				} else {
					window.contentLoadError($(this));
				}
			});
			
			$video.children('source').on('error', function() {
					/* Initial video load tends to trigger an error on the source */
					/* Kind of annoying */
				window.contentLoadError($(this));
			});
					
			$video.on('ended', function() {
					/* Clear the freezeLoop first */
				clearInterval($(this).data('freezeLoop'));

					/* Resume the slideshow */
				var $show = $(this).closest('.cycle-slideshow');
				$show.cycle('resume');

					/* Stop blocking refreshes */
				window.cfg.deferRefreshes = false;
			});				
		});
		
		$content.find('.embedded-layout').each(function() {
			var desig = $(this).data('sign-hint');
			var height = $region.height();
			var width = $region.width();
		
			$(this).append('<iframe src="' + getSignURL(desig) + '" height="' + height + '" width="' + width + '"></iframe>');
		});
		
		$container.append($content);
	});

		/* Every region either overrides backup content or overlays backup content */
		/* Only remove from regions that are specified to override backup content */
	$(".region.backup-override").each(function() {
		if($(this).find(".not-backup").length != 0)
			$(this).find(".backup").remove();
	});
	
	
	$(".slideshow-container section.announcement").on('do-scale', function() {
		var regHeight = $(this).closest(".region").get(0).offsetHeight;
		var $affect = $(this).find('p.sign-text');
		var elem = $(this).get(0);
		
		var fontSize = parseInt($affect.css('font-size'));
		var lastGood = 0;
		
		while(elem.offsetHeight < regHeight) {
			lastGood = fontSize++;
			$affect.css('font-size', fontSize + 'px');
		}
		
		$affect.css('font-size', lastGood);
	});
	
	$(".slideshow-container").off('cycle-bootstrap').on('cycle-bootstrap', function (e, opts, api) {
		if(window.cfg.cycleHacksEnabled) {
			if(typeof api._calcNextSlide !== 'function')
				api._calcNextSlide = api.calcNextSlide;
			
			if(typeof api._updateView !== 'function')
				api._updateView = api.updateView;

			api.calcNextSlide = window.myCalcNextSlide;
			api.updateView = window.myUpdateView;
		}
	});
	
	var $allSlides = $(".region.type-slideshow .content.slide");
		
	$allSlides.on('overflow-start', function(evt) {
		var idx = 0;
		var lastTop = 0;
		var timeReq = 0;
		var queueNames = [];
	
		var series = (new Date).getTime();
		
		var $slide = $(this);
		
		if($slide.find("section.announcement").length != 0)
			return false;

		var $region = $slide.closest(".region");
		var regionName = $region.data('region-folder');
		var overflow = ($slide.height() - $region.height());
		
		$slide.clearQueue();

		$slide.find("article").each(function() {
			var position = $(this).position();
			var top = Math.max(position.top, lastTop)
			lastTop = top;
			
			var txt = $(this).text();
			var timeout = countWords(txt) / window.cfg.wordsPerSecond;

			if((overflow - (top - $(this).height())) <= 0) 
				return false;
			
			var name = "overflow" + idx + Now();
			idx += 1;
			queueNames.push(name);
			timeReq += (timeout * 1000);

			$slide.animate({
				top: "-" + top + "px",
			},
			{
				duration: 500,
				queue: name,
				easing: 'linear',
				complete: function() {
					setTimeout(function() {
							/* Only advance if this is still the current series */
						if($region.data('overflow-series') == series)
							$slide.trigger('overflow-advance', [series]);
					}, (timeout * 1000));
				},
			});
		});

		$slide.data('overflow-time-req', timeReq);

		if(queueNames.length != 0) {
			console.log('[overflow] [' + regionName + '] start (' + series + ')');
			$slide.data('queueNames', queueNames);
			$region.data('overflow-series', series);
			
			dAssert($slide.length == 1);
			
			setTimeout(function() {
				$slide.trigger('overflow-advance', [series]);
			}, 2000);
		}
	});

	$allSlides.on('overflow-kill', function(evt, series) {
		var $slide = $(this);
		var $region = $(this).closest('.region');
		
		$region.data('overflow-series', null);
		$slide.data('queueNames', null);
		$slide.clearQueue();

		var regionName = $region.data('region-folder');
		
		console.log('[overflow] [' + regionName + '] overflow-kill (' + series + ')');
	});
	
	$allSlides.on('overflow-advance', function(evt, series) {
		var $slide = $(this);
		var queueNames = $slide.data('queueNames');
		var $region = $slide.closest('.region');
		var regionName = $region.data('region-folder');
		var currentSeries = $region.data('overflow-series');

		console.log('[overflow] [' + regionName + '] overflow-advance (' + series + ') (current: ' + currentSeries + ')');
		
		var isCurrent = (currentSeries && currentSeries == series);
		
		if(isCurrent && typeof queueNames !== 'undefined' && queueNames && queueNames.length != 0) {
			var nextElem = queueNames.shift();
			$slide.data('queueNames', queueNames);
			
			$slide.dequeue(nextElem);
		} else if(isCurrent && $slide.siblings(".slide:not(.cycle-sentinel)").length == 0) {
			console.log('[overflow] [' + regionName + '] rewind');
			var timeLeft = $slide.data('cycle-timeout') - $slide.data('overflow-time-req');
			
			setTimeout(function() {
				$slide.animate({
					top: "0px",
				},
				{
					duration: 500,
					easing: 'linear',
					complete: function() {
						$slide.trigger('overflow-start');
					},
				});			
			}, timeLeft);
		} else {
			console.log('[overflow] [' + regionName + '] finished (' + series + ')');
		}
	});

	$(".region.type-slideshow > .slideshow-container").cycle({
		slides: "> .slide",
		firstRun: true,
	}).addClass("cycle-slideshow").click(function() {
		$(this).cycle("next");	
	}).off('cycle-update-view cycle-after cycle-before');
		
	var $marquees = $(".region.type-marquee > .container");
	
	$marquees.marquee();
	
	$('.cycle-slideshow').each(function() {
		var $show = $(this);
		
		$show.on('cycle-update-view', function(evt, showOpts, slideOpts, curSlide) {
			var $slide = $(curSlide);
			var slideLock = $slide.data('slideLock');

			$(curSlide).find('section').trigger('do-scale');

			if(slideOpts['slideNum'] == 1 && (typeof slideLock == 'undefined' || !slideLock)) {
				var $outSlide = $slide.siblings(".slide:not(.cycle-sentinel)").filter(':last-of-type');
				var $region = $slide.closest(".region");

				$slide.data('slideLock', true);
				var overflow = ($slide.height() - $show.closest(".region").height());

				if($outSlide.length != 0)
					$outSlide.trigger('overflow-kill', [$region.data('overflow-series')]);
				
				if(overflow > 0) {
					$slide.trigger('overflow-start');
				}
			}

			if(showOpts['firstRun'])
				showOpts['firstRun'] = false;
			
			return true;
		});
		
		$show.on('cycle-after', function(evt, slideOpts, outSlide, curSlide, ffFlag) {
			var $outSlide = $(outSlide);
			var $region = $show.closest(".region");
			$outSlide.data('slideLock', false);

			if(slideOpts['slideNum'] != 1) {
				$outSlide.trigger('overflow-kill', [$region.data('overflow-series')]);

				var $slide = $(curSlide);
				var showOpts = $show.cycle('opts');
				var overflow = ($slide.height() - $region.height());

				if(overflow > 0)
					$slide.trigger('overflow-start');
			}
		});
		
		if($show.data('contains-video') === true) {
			$show.on('cycle-before', function(evt, opts, outgoingSlideEl, incomingSlideEl, ffFlag) {
				var $slide = $(incomingSlideEl);
				if($slide.data('contains-video') === true) {
					var $video = $slide.find('video');

					if($video.data('ready') !== true) {
						opts.API.log('Skipping slide, video not ready');
						opts.skip = true;
					}
				}
			});
			
			$show.on('cycle-after', function(event, opts, outgoingSlideEl, incomingSlideEl, ffFlag) {
				var $slide = $(incomingSlideEl);
				if($slide.data('contains-video') === true) {
						/* Defer all refreshing until we get done playing */
					window.cfg.deferRefreshes = true;
					
					var $video = $slide.find('video');
					
						/* Set the timeout to 0, so the transition happens as soon as we resume */
						/* Pause the slideshow until the video is over */
					opts.timeout = 0;
					$show.cycle('pause');
										
					var video = $video.get(0);
			
					$video.data('position', 0);
					
						/* Ensure the video isn't frozen, every 2.5 seconds */
					$video.data('freezeLoop', setInterval(function() {
						var curPos = video.currentTime;
						var lastPos = $video.data('position');
						
						if(curPos == lastPos) {
								/* The video appears to have frozen- pause it, then trigger the 'ended' */
							video.pause();
							$video.trigger('ended');
							
								/* Previously, we reset it to position 0...I'd like to think it's an error */
								/* As such, trigger the error state so that it will be removed from the show */
								/* And then promptly reloaded */
							$video.trigger('error');
						}
						
						$video.data('position', curPos);
					}, 4000));
					
						/* This was fun, we should definitely do this again sometime */
					//video.playbackRate = 32;
					video.play();
				}
			});
		}
	});	
	
	window.content.building = false;
};

$(document).ready(function() {
	window.content = new Refreshable("pull.php", "content-cache", window.contentReceived, window.cfg.contentAllowedAge);
	
	$(window.content).on('alter-cache-key', function(evt) {
		var $main = $("#main");

		if($main.length != 0)
			evt.key += "__l" + $main.data('layout') + '__';
		else
			evt.key += "__init__"
	});
	
	$(window.content).on('alter-request', function(evt) {
		var $main = $("#main");
		evt.request += "&time=" + (window.layoutStart || window.cronStart || window.signStart);
		
		if($main.length != 0)
			evt.request += "&layout=" + $main.data('layout');
	});
	
	$(window.content).on('after-refresh', function(evt, success) {
		if(success)
			window.contentStart = adjNow();
	});
	
	window.content.isForced = function() {
		return window.cfg.force_content;
	};
	
	if(window.layout) {
			/* If the layout updates, we'll do a cold refresh (= refresh from cache) of the content */
		window.layout.postReceiveData = function() {
			
				/* EXCEPTION: If we're scheduled to run this tick, let's not do this */
			if(!window.content.willRun(window.cfg.currentTick)) 
				window.content.coldRefresh();
		}
	}
});