<?php

$referrer = $_SERVER['HTTP_REFERER'];
$refParts = parse_url($referrer);
 
$sessKey = $refParts['path'] . '?' . $refParts['query'];

session_name($sessKey);
session_start();
/*
if(isset($_SESSION[$sessKey]))
	$sessVars =& $_SESSION[$sessKey];
else
	$sessVars = array();
*/
$sessVars =& $_SESSION;

header('Content-Type: text/javascript');

if(empty($_GET['file']))
	die('');

$jsFile = $_GET['file'];

if(!file_exists($jsFile)) {
	$jsFile .= '.js';

	if(!file_exists($jsFile))
		die('');
}

ob_start();
require_once('./' . $jsFile);

$js = ob_get_contents();
ob_end_clean();

switch($jsFile) {
	case 'sign-remote-init.js':
		$initLines = array();
		$optionalVars = array(
							'window.region' => 'region',
							'window.regionFg' => 'fg',
							'window.regionBg' => 'bg',
							);


		if(isset($sessVars['sign']))
			$sign = $sessVars['sign'];
		else
			$sign = 'sign-dev';

		$initLines[] = "window.labDesignator = '{$sign}'";
		$initLines[] = 'window.preview = true';
		$initLines[] = '$("body").addClass("preview")';
	
		foreach($optionalVars as $jsVar => $sessVar) {
			if(isset($sessVars[$sessVar]))
				$initLines[] = "{$jsVar} = '{$sessVars[$sessVar]}'";
		}

		$initLines = implode(";\r\n\t", $initLines) . ";";	
		$js = "$(document).ready(function() {
	{$initLines}
});";
		
		break;
	default:
		break;
}

echo "// REF: " . $referrer . "\r\n";
echo $js;
