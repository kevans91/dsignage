function intranetTimeFormat() {
	return 'H:mm';
}

function intranetDateFormat() {
	return 'YYYY-MM-DD';
}

function isToday(startDate, endDate) {
	var start = moment(startDate, intranetDateFormat());
	var end = moment(endDate, intranetDateFormat());
	var now = moment().dayOfYear();
	
	return (start.dayOfYear() < now && end.dayOfYear() > now); 
}

function shortTimeFormat() {
	return 'h:mm';
}

function longTimeFormat() {
	return shortTimeFormat() + ' A';
}

function sameMeridiem(moment1, moment2) {
	var m1h = moment1.hour();
	var m2h = moment2.hour();
	
	return ((m1h < 12 && m2h < 12) || (m1h > 11 && m2h > 11)); 
}

function fullDateFormat() {
	return 'MMMM D';
}

function fullDateYearFormat() {
	return fullDateFormat() + ', YYYY';
}

function weekdayFormat() {
	return 'dddd';
}

function shortDateFormat() {
	return 'D';
}

function isSameWeek(moment1, moment2) {
	return (moment1.week() === moment2.week());
}

function isSameMonth(moment1, moment2) {
	return (moment1.month() === moment2.month());
}

function getDate() {
	return moment().format(fullDateYearFormat());
}

function getTime() {
	return moment().format('h:mm A');
}

function parseEventDates(startDate, endDate) {
	var start = moment(startDate, intranetDateFormat());
	var end = moment(endDate, intranetDateFormat());
	var today = moment();
	
	var startEndRange = end.dayOfYear() - start.dayOfYear();
	var startTodayRange = today.dayOfYear() - start.dayOfYear();
	var todayEndRange = startEndRange - startTodayRange;
	
	var dateStr = '';
	
	if(todayEndRange < 0) {
				// Event ended
		if(!startEndRange) {
					// Start and end on the same day, only display one point in time.
			return start.format(fullDateFormat());
		} else {
			var dateStr = start.format(fullDateFormat() + ' - ');
		
			if(isSameMonth(start, end)) {
				dateStr += end.format(shortDateFormat());
			} else {
				dateStr += end.format(fullDateFormat());
			}
			
			return dateStr;
		}
	} else if(!todayEndRange) {
				// Today is the last date of the event
		return 'Today';
	} else if(todayEndRange === 1) {
				// Tomorrow is the last day of the end
		if(startEndRange === 0) {
			return 'Tomorrow';
		} else {
			return 'Today &amp; Tomorrow';
		}
	} else {
			// Future event
		if(startEndRange === 0) {
				// Single point in time
			if(isSameWeek(today, end)) {
				return end.format(weekdayFormat());
			}
			
			return end.format(fullDateFormat());						
		}
		
		if(startTodayRange >= 0) {
				// Event has already started.
			dateStr = 'Today';
		} else if(startTodayRange === -1) {
			dateStr = 'Tomorrow';
		} else if(isSameWeek(today, start)) {
			dateStr = start.format(weekdayFormat());
		} else {
			dateStr = start.format(fullDateFormat());
		}
		
		dateStr += ' - ';
		
		if(isSameWeek(today, end)) {
			dateStr += end.format(weekdayFormat());
		} else if(startTodayRange < 1 && isSameMonth(start, end) && startTodayRange < -1) {
			dateStr += end.format(shortDateFormat());
		} else {
			dateStr += end.format(fullDateFormat());
		}
		
		return dateStr;
	}
}

function parseEventDate(d) {
	var then = moment(d, intranetDateFormat());
	var today = moment();

	if(isSameWeek(then, today)) {
		return then.format(weekdayFormat());
	} else {
		return then.format(fullDateFormat());
	}
}

function parseEventTimes(startTime, endTime) {
	if(!startTime || (typeof (startTime) === 'undefined') || !startTime.length) return '';
	
	var start = moment(startTime, intranetTimeFormat());
	
	if(!endTime || (typeof (endTime) === 'undefined') || !endTime.length) return start.format(longTimeFormat());

	var end = moment(endTime, intranetTimeFormat());

	if(sameMeridiem(start, end)) {
		return start.format(shortTimeFormat()) + ' to ' + end.format(longTimeFormat());
	}
	
	return start.format(longTimeFormat()) + ' to ' + end.format(longTimeFormat());
}