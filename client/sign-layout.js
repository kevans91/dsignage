/**
	sign-layout.js by Kyle Evans
	
	Purpose: Layout building functionality for the signs
**/

var backgrounds = [];
var background = "";

window.buildLayout = function(dataObject) {
		/* Need to add background-image to #main */
		/* And orientation class */
	window.layout.building = true;
	
	if(dataObject !== undefined && dataObject.id !== undefined) {
		var $body = $('body');
		
		var curLayout = -1;
		var newLayout = dataObject['id'];
		
		if($("#main").length != 0)
			curLayout = $("#main").data('layout');
		
		var $container = $('<div id="main"></div>');
		
		$container.css('width', dataObject['width'] + 'px');
		$container.css('height', dataObject['height'] + 'px');
		$container.addClass('layout-' + idify(dataObject['title']));
		$container.data('layout', newLayout);
		
			/* Omit the background if we're previewing a region */
		if(!window.region) {
			backgrounds = dataObject['bg'].split(':');
			rotate = (dataObject['rotate'] == 't');

			if(background == "" || rotate || curLayout != newLayout) {
				if(background == "")
					background = backgrounds[0];
				else
					background = backgrounds[(backgrounds.indexOf(background) + 1) % backgrounds.length];
			}
			
			$container.css('background-image', 'url(' + bgPath + background + ')');
		}
		
		$.each(dataObject['regions'], function(k, v) {
			var idified = idify(v['name']);
			var $region = $('<div id="region-' + idified + '" class="region" data-region-folder="' + idified + '"></div>');
			
			if(v['height'])
				$region.css('height', v['height'] + 'px');

			if(v['width'])
				$region.css('width', v['width'] + 'px');
			
				/* Omit positioning of regions if we're doing a region preview */
			if(!window.region) {
				if(v['top']) $region.css('top', v['top'] + 'px');
				if(v['right']) $region.css('right', v['right'] + 'px');
				if(v['bottom']) $region.css('bottom', v['bottom'] + 'px');
				if(v['left']) $region.css('left', v['left'] + 'px');
			}
			
			bg = null;
			fg = null;
			
			if(window.regionBg)
				bg = window.regionBg;
			else if(v['bgcolor'])
				bg = v['bgcolor'];

			if(window.regionFg)
				fg = window.regionFg;
			else if(v['fgcolor'])
				fg = v['fgcolor'];
			
			if(bg) $region.css('background-color', bg);
			if(fg) $region.css('color', fg);
			if(v['fontsize']) $region.css('font-size', v['fontsize'] + 'px');
			if(v['lineheight']) $region.css('line-height', v['lineheight'] + 'px');
			if(v['css']) $region.attr('style', $region.attr('style') + ';' + v['css']);
			if(v['children_css']) $region.data('child-css', v['children_css']);
			if(v['overlays_backup'] && v['overlays_backup'] == 't') $region.addClass('backup-overlay');
			else $region.addClass('backup-override');
			
			var _class = '';

			if(v['class']) 
				_class = v['class'];
			else if(v['desc'])
				_class = idify(v['desc']);
			else
				_class = 'unknown';

			_class = _class.toLowerCase();
			
			$region.addClass('type-' + _class);

				/* Adding a -container isn't strictly necessary, but it is kind of nice */
			$region.append('<div class="' + _class + '-container container" style="width: ' + $region.width() + 'px; height: ' + $region.height() + 'px;"></div>');

			if($region.hasClass('type-date') || $region.hasClass('type-time')) {
				var $rContainer = $region.children('.container');
				
				if($region.hasClass('type-date'))
					$rContainer.text(window.getDate());
				else
					$rContainer.text(window.getTime());
			}
			
			$container.append($region);
		});

		$body.children().remove();		/* Nuke anything that's there */
		$body.append($container);
	}
	window.layout.building = false;
}

$(document).ready(function() {
	window.layout = new Refreshable("layout.php", "layout-cache", window.buildLayout, window.cfg.layoutAllowedAge);
	
	$(window.layout).on('alter-request', function(evt) {
		var $main = $("#main");
		
		if($main.length != 0)
			evt.request += "&layout=" + $main.data('layout');
	});
	
	$(window.layout).on('after-refresh', function(evt, success) {
		if(success)
			window.layoutStart = (window.cronStart || window.signStart);
	});
	
	window.layout.isForced = function() {
		return window.cfg.force_layout;
	};
});